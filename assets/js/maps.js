function showWithMarker(latitude, longitude, zoom_map){  

      var myLatLng = {lat: latitude, lng: longitude};

      var map = new google.maps.Map(document.getElementById('maps'), {
        zoom: zoom_map,
        center: myLatLng
      });

      marker = new google.maps.Marker(
      {
          map:map,
          draggable:true,
          animation: google.maps.Animation.DROP,
          position: myLatLng
      });
      google.maps.event.addListener(marker, 'dragend', function() 
      {
          geocodePosition(marker.getPosition());
      });

    }

    function geocodePosition(pos) 
    {
       geocoder = new google.maps.Geocoder();
       geocoder.geocode
        ({
            latLng: pos
        }, 
            function(results, status) 
            {
                if (status == google.maps.GeocoderStatus.OK) 
                {
                    $("#alamat").val(results[0].formatted_address);
                    $("#latitude").html(results[0].geometry.location.lat);
                    $("#longitude").html(results[0].geometry.location.lng);
                    $(".latitude").val(results[0].geometry.location.lat);
                    $(".longitude").val(results[0].geometry.location.lng);
                } 
                else 
                {
                    $("#alamat").val('Cannot determine address at this location.'+status);
                }
            }
        );
    }