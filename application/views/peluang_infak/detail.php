<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Top Navigation</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/font-awesome/css/font-awesome.min.css"); ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/ionicons/css/ionicons.min.css"); ?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.min.css"); ?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/skins/_all-skins.min.css"); ?>">

        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css'); ?>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="hold-transition skin-blue layout-top-nav">
        <div class="wrapper">

            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="../../index2.html" class="navbar-brand"><b>Admin</b>LTE</a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>
                                <li><a href="<?php echo site_url("user_registration"); ?>">Registration</a></li>
                                <li class="active"><a href="<?php echo site_url("peluang_infak"); ?>">Peluang Infak <span class="sr-only">(current)</span></a></li>
                                <li><a href="<?php echo site_url("auth"); ?>">Login</a></li>
                            </ul>
                        </div>
                        <!-- /.navbar-custom-menu -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </header>

            <!-- Full Width Column -->
            <div class="content-wrapper">
                <div class="container">
                    <!-- Content Header (Page header) -->
                    <!-- <section class="content-header">
                        <h1>Top Navigation <small>Example 2.0</small></h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="#">Layout</a></li>
                            <li class="active">Top Navigation</li>
                        </ol>
                    </section> -->

                    <!-- Main content -->
                    <section class="content">

						<!-- TABLE: LATEST ORDERS -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">KSBM</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									<!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
								</div>
							</div><!-- /.box-header -->
							<div class="box-body">

                                <div class="row">
                                    <div class="col-md-2">
                                        Kegiatan
                                    </div>
                                    <div class="col-md-10">
                                        <?php echo $row->KEAHLIAN; ?>
                                    </div>

                                    <div class="col-md-2">
                                        Masjid
                                    </div>
                                    <div class="col-md-10">
                                        <?php echo $row->NAMA; ?>
                                    </div>

                                    <div class="col-md-2">
                                        Alamat
                                    </div>
                                    <div class="col-md-10">
                                        <?php echo $row->ALAMAT; ?>
                                    </div>

                                    <div class="col-md-2">
                                        Biaya Trainer
                                    </div>
                                    <div class="col-md-10">
                                        <?php echo number_format($row->BIAYA_TRAINER,0,",","."); ?>
                                    </div>

                                    <div class="col-md-2">
                                        Biaya Konsumsi
                                    </div>
                                    <div class="col-md-10">
                                        <?php echo number_format($row->BIAYA_KONSUMSI,0,",","."); ?>
                                    </div>

                                    <div class="col-md-2">
                                        Biaya Alat
                                    </div>
                                    <div class="col-md-10">
                                        <?php echo number_format($row->BIAYA_ALAT,0,",","."); ?>
                                    </div>

                                    <div class="col-md-2">
                                        Total Biaya
                                    </div>
                                    <div class="col-md-10">
                                        <?php echo number_format($row->KEBUTUHAN, 0,",","."); ?>
                                    </div>

                                    <div class="col-md-2">
                                        No. Rekening
                                    </div>
                                    <div class="col-md-10">
                                        No. Rekening
                                    </div>

                                    <div class="col-md-12">
                                        <a href="<?php echo site_url("auth"); ?>" class="btn btn-primary">Lapor</a>
                                    </div>
                                </div>

							</div><!-- /.box-body -->
						</div><!-- /.box -->

					</section>
                    <!-- /.content -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="container">
                    <div class="pull-right hidden-xs">
                        <b>Version</b> 2.4.0
                    </div>
                    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights reserved.
                </div>
                <!-- /.container -->
            </footer>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url("assets/dist/js/app.min.js"); ?>"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url("assets/dist/js/demo.js"); ?>"></script>

        <!-- DataTables -->
        <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>

		<script type="text/javascript">
		$(document).ready(function(){
            var table_pi = $("#table-peluang-infak").DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url($modul.'/ajax_list')?>",
                    "type": "POST"
                },

                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        "targets": [ 0 ], //first column
                        "orderable": false, //set not orderable
                    },
                    {
                        "targets": [ -1 ], //last column
                        "orderable": false, //set not orderable
                    }
                ]
            });
		});
		</script>
    </body>
</html>
