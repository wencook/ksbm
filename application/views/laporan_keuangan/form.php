<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Laporan Pengeluaran</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="<?php echo site_url($modul."/".$url); ?>" method="post">
            <div class="form-group">
                <label for="opt_kegiatan" class="control-label col-sm-2">Kegiatan</label>
                <div class="col-sm-10">
                    <select class="form-control" name="opt_kegiatan" id="opt_kegiatan" autofocus="autofocus">
                        <option value="">Pilih Kegiatan</option>
                        <?php
                        foreach ($res_kegiatan as $v)
                        {
                            ?>
                            <option value="<?php echo $v->ID; ?>"><?php echo $v->TITLE; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="txt_trainer" class="control-label col-sm-2">Trainer</label>
                <div class="col-sm-10">
                    <span id="nama_trainer"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="txt_biaya_keluar" class="control-label col-sm-2">Biaya Keluar</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_biaya_keluar" id="txt_biaya_keluar" class="form-control" placeholder="Biaya Keluar" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_tanggal" class="control-label col-sm-2">Tanggal</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_tanggal" id="txt_tanggal" class="form-control" placeholder="Tanggal" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="opt_keperluan" class="control-label col-sm-2">Keperluan</label>
                <div class="col-sm-10">
                    <select class="form-control" name="opt_keperluan" id="opt_keperluan">
                        <option value="Biaya Konsumsi">Biaya Konsumsi</option>
                        <option value="Biaya Trainer">Biaya Trainer</option>
                        <option value="Biaya Alat">Biaya Alat</option>
                        <option value="Lain-Lain">Lain-Lain</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="txt_catatan" class="control-label col-sm-2">Catatan</label>
                <div class="col-sm-10">
                    <textarea name="txt_catatan" id="txt_catatan" class="form-control" rows="2" placeholder="Catatan"></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" name="btn_save" value="save">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $("#txt_tanggal").datepicker({
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $("#opt_kegiatan").change(function(){
        var val_kegiatan = $("#opt_kegiatan").val();
        $.ajax({
            url: "<?php echo site_url($modul."/ajax_trainer"); ?>",
            data: {id_kegiatan: val_kegiatan},
            method: "post",
            success : function(data) {
                $("#nama_trainer").html(data);
            }
        });
    });
});
</script>
