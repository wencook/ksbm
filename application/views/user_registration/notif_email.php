<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <p>Klik link berikut untuk verifikasi email anda <?php echo site_url("user_registration/aktivasi/".$row->ID_UR); ?></p>
        <p>Data registrasi</p>
        <table>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td><?php echo $row->NAME; ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td><?php echo $row->EMAIL; ?></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td><?php echo $row->name_kelurahan." ".$row->name_kecamatan." ".$row->name_kabupaten." ".$row->name_provinsi; ?></td>
            </tr>
            <tr>
                <td>Keahlian</td>
                <td>:</td>
                <td>
                    <ul>
                        <?php
                        $this->db->select("k.KEAHLIAN");
                        $this->db->where("USER_REGISTRATION_ID", $row->ID_UR);
                        $this->db->join("KEAHLIAN_TBL k", "urk.KEAHLIAN_ID = k.ID");
                        $qry = $this->db->get("USER_REGISTRATION_KEAHLIAN_TBL urk")->result();
                        if ($qry)
                        {
                            foreach ($qry as $value)
                            {
                                ?>
                                <li><?php echo $value->KEAHLIAN; ?></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </td>
            </tr>
        </table>
    </body>
</html>
