<!-- Full Width Column -->
<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <!-- <section class="content-header">
            <h1>Top Navigation <small>Example 2.0</small></h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section> -->

        <!-- Main content -->
        <section class="content">

			<!-- TABLE: LATEST ORDERS -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">KSBM</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						<!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
					</div>
				</div><!-- /.box-header -->
				<div class="box-body">

			        <form class="form-horizontal" method="post" action="<?php echo site_url($modul."/save_registration"); ?>">
			            <div class="form-group">
			                <label for="txt_name" class="col-sm-2 control-label">Nama</label>
			                <div class="col-sm-10">
			                    <input type="text" class="form-control" name="txt_name" id="txt_name" placeholder="Nama" autocomplete="off" autofocus="autofocus">
			                </div>
			            </div>

			            <div class="form-group">
			                <label for="txt_email" class="col-sm-2 control-label">Email</label>
			                <div class="col-sm-10">
			                    <input type="email" class="form-control" name="txt_email" id="txt_email" placeholder="Email" autocomplete="off">
			                </div>
			            </div>

						<div class="form-group">
			                <label for="txt_hp" class="col-sm-2 control-label">HP</label>
			                <div class="col-sm-10">
			                    <input type="text" class="form-control" name="txt_hp" id="txt_hp" placeholder="HP" autocomplete="off">
			                </div>
			            </div>

			            <div class="form-group">
			                <label for="opt_provinsi" class="col-sm-2 control-label">Provinsi</label>
			                <div class="col-sm-10">
			                    <select class="form-control" name="opt_provinsi" id="opt_provinsi">
			                        <option value="">Pilih Provinsi</option>
			                        <?php
			                        if ($res_provinsi)
			                        {
			                            foreach ($res_provinsi as $val)
			                            {
			                                ?>
			                                <option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
			                                <?php
			                            }
			                        }
			                        ?>
			                    </select>
			                </div>
			            </div>

			            <div class="form-group">
			                <label for="opt_kabupaten" class="col-sm-2 control-label">Kabupaten</label>
			                <div class="col-sm-10">
			                    <select class="form-control" name="opt_kabupaten" id="opt_kabupaten">
			                        <option value="">Pilih Kabupaten</option>
			                    </select>
			                </div>
			            </div>

			            <div class="form-group">
			                <label for="opt_kecamatan" class="col-sm-2 control-label">Kecamatan</label>
			                <div class="col-sm-10">
			                    <select class="form-control" name="opt_kecamatan" id="opt_kecamatan">
			                        <option value="">Pilih Kecamatan</option>
			                    </select>
			                </div>
			            </div>

						<div class="form-group">
			                <label for="opt_kelurahan" class="col-sm-2 control-label">Kelurahan / Desa</label>
			                <div class="col-sm-10">
			                    <select class="form-control" name="opt_kelurahan" id="opt_kelurahan">
			                        <option value="">Pilih Kelurahan/Desa</option>
			                    </select>
			                </div>
			            </div>

                        <div class="form-group">
                            <label for="chk_keahlian" class="col-sm-2 control-label">Keahlian / Peminatan</label>
			                <div class="col-sm-10">
                                <div class="checkbox">
                                        <?php
                                        foreach ($res_keahlian as $vkeahlian)
                                        {
                                            ?>
                                            <label>
                                                <input type="checkbox" name="chk_keahlian[<?php echo $vkeahlian->ID; ?>]" id="chk_keahlian" value="<?php echo $vkeahlian->ID; ?>"><?php echo $vkeahlian->KEAHLIAN; ?>
                                            </label><br>
                                            <?php
                                        }
                                        ?>
                                </div>
                                <span id="helpBlock" class="help-block">Maksimal Pilih 3</span>
			                </div>
			            </div>

                        <div class="form-group">
			                <label for="opt_peran" class="col-sm-2 control-label">Peran</label>
			                <div class="col-sm-10">
                                <select class="form-control" name="opt_peran" id="opt_peran">
                                    <option value="">Pilih Peran</option>
                                    <?php
                                    foreach ($res_peran as $vperan)
                                    {
                                        ?>
                                        <option value="<?php echo $vperan->ID; ?>"><?php echo $vperan->PERAN; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
			                </div>
			            </div>

						<div class="form-group">
							<label for="txt_captcha" class="col-sm-2 control-label"><?php echo $cap['image']; ?></label>
			                <div class="col-sm-10">
			                    <input type="text" class="form-control" name="txt_captcha" id="txt_captcha" placeholder="Captcha" required="required" autocomplete="off">
			                </div>
						</div>

			            <div class="form-group">
			                <div class="col-sm-offset-2 col-sm-10">
			                    <button type="submit" class="btn btn-primary" name="btn_save" value="save">Save</button>
			                </div>
			            </div>
			        </form>

				</div><!-- /.box-body -->
			</div><!-- /.box -->

		</section>
        <!-- /.content -->
    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->