<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Top Navigation</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/font-awesome/css/font-awesome.min.css"); ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/ionicons/css/ionicons.min.css"); ?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.min.css"); ?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/skins/_all-skins.min.css"); ?>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="hold-transition skin-blue layout-top-nav">
        <div class="wrapper">

            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="../../index2.html" class="navbar-brand"><b>Admin</b>LTE</a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                                <li class="active"><a href="<?php echo site_url("user_registration"); ?>">Registration</a></li>
                                <li><a href="<?php echo site_url("peluang_infak"); ?>">Peluang Infak <span class="sr-only">(current)</span></a></li>
                                <li><a href="<?php echo site_url("auth"); ?>">Login</a></li>
                            </ul>
                        </div>
                        <!-- /.navbar-custom-menu -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </header>

            <!-- Full Width Column -->
            <div class="content-wrapper">
                <div class="container">
                    <!-- Content Header (Page header) -->
                    <!-- <section class="content-header">
                        <h1>Top Navigation <small>Example 2.0</small></h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="#">Layout</a></li>
                            <li class="active">Top Navigation</li>
                        </ol>
                    </section> -->

                    <!-- Main content -->
                    <section class="content">

                        <div class="box box-info">
                        	<div class="box-header with-border">
                        		<h3 class="box-title">KSBM</h3>
                        		<div class="box-tools pull-right">
                        			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        			<!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        		</div>
                        	</div><!-- /.box-header -->
                        	<div class="box-body">
                                <h1>Email telah diverifikasi, data anda akan diproses pihak administrator</h1>
                                <p>Klik <a href="<?php echo base_url("user_registration"); ?>">disini</a> untuk register.</p>
                                <p>Klik <a href="<?php echo base_url("auth"); ?>">disini</a> untuk login</p>
                            </div>
                        </div>

					</section>
                    <!-- /.content -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="container">
                    <div class="pull-right hidden-xs">
                        <b>Version</b> 2.4.0
                    </div>
                    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights reserved.
                </div>
                <!-- /.container -->
            </footer>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url("assets/dist/js/app.min.js"); ?>"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url("assets/dist/js/demo.js"); ?>"></script>

		<script type="text/javascript">
		$(document).ready(function(){
			$("#opt_provinsi").change(function(){
				var val_provinsi = $("#opt_provinsi").val();
				$.ajax({
					url: "<?php echo site_url($modul."/ajax_kabupaten"); ?>",
					data: {id_provinsi: val_provinsi},
					method: "post",
					success: function(data) {
						$("#opt_kabupaten").html(data);
					}
				});
			});

			$("#opt_kabupaten").change(function(){
				var val_kabupaten = $("#opt_kabupaten").val();
				$.ajax({
					url: "<?php echo site_url($modul."/ajax_kecamatan"); ?>",
					data: {id_kabupaten: val_kabupaten},
					method: "post",
					success: function(data) {
						$("#opt_kecamatan").html(data);
					}
				});
			});

			$("#opt_kecamatan").change(function(){
				var val_kecamatan = $("#opt_kecamatan").val();
				$.ajax({
					url: "<?php echo site_url($modul."/ajax_kelurahan"); ?>",
					data: {id_kecamatan: val_kecamatan},
					method: "post",
					success: function(data) {
						$("#opt_kelurahan").html(data);
					}
				});
			});
		});
		</script>
    </body>
</html>
