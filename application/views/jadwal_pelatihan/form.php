<?php
$id = isset($r_training) ? $r_training->ID : "";
$mosque = isset($r_training) ? $r_training->MOSQUE_ID : "";
$bidang = isset($r_training) ? $r_training->BIDANG_ID : "";
$user = isset($r_training) ? $r_training->USER_REGISTRATION_ID : "";
$course = isset($r_training) ? $r_training->COURSE_CATALOGUE_ID : "";
$date = isset($r_training) ? $r_training->DATE : "";
$biaya_trainer = isset($r_training) ? $r_training->BIAYA_TRAINER : "";
$biaya_konsumsi = isset($r_training) ? $r_training->BIAYA_KONSUMSI : "";
$biaya_alat = isset($r_training) ? $r_training->BIAYA_ALAT : "";
$val_btn = isset($r_training) ? "update" : "save";
?>

<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Daftar Masjid</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="<?php echo site_url($modul."/".$url); ?>" method="post">
            <input type="hidden" name="txt_id" value="<?php echo $id; ?>">
            <div class="form-group">
                <label for="opt_mosque" class="control-label col-sm-2">Nama Masjid</label>
                <div class="col-sm-10">
                    <select class="form-control" name="opt_mosque" id="opt_mosque" autofocus="autofocus">
                        <option value="">Pilih Masjid</option>
                        <?php
                        foreach ($res_mosque as $vm)
                        {
                            $sel_mosque = $vm->ID == $mosque ? "selected=\"selected\"" : "";
                            ?>
                            <option value="<?php echo $vm->ID; ?>" <?php echo $sel_mosque; ?>><?php echo $vm->NAMA; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="opt_bidang" class="control-label col-sm-2">Bidang</label>
                <div class="col-sm-10">
                    <select class="form-control" name="opt_bidang" id="opt_bidang">
                        <option value="">Pilih Bidang</option>
                        <?php
                        if ($res_keahlian)
                        {
                            foreach ($res_keahlian as $val)
                            {
                                $sel_keahlian = $val->ID == $bidang ? "selected=\"selected\"" : "";
                                ?>
                                <option value="<?php echo $val->ID; ?>" <?php echo $sel_keahlian; ?>><?php echo $val->KEAHLIAN; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="opt_user" class="control-label col-sm-2">Trainer</label>
                <div class="col-sm-10">
                    <select class="form-control" name="opt_user" id="opt_user">
                        <option value="">Pilih Trainer</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="opt_course" class="control-label col-sm-2">Course</label>
                <div class="col-sm-10">
                    <select class="form-control" name="opt_course" id="opt_course">
                        <option value="">Pilih Course</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="txt_date" class="control-label col-sm-2">Tanggal</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_date" id="txt_date" class="form-control" placeholder="Tanggal" value="<?php echo $date; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_biaya_trainer" class="control-label col-sm-2">Biaya Trainer</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_biaya_trainer" id="txt_biaya_trainer" class="form-control" placeholder="Biaya Trainer" value="<?php echo $biaya_trainer; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_biaya_konsumsi" class="control-label col-sm-2">Biaya Konsumsi</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_biaya_konsumsi" id="txt_biaya_konsumsi" class="form-control" placeholder="Biaya Konsumsi" value="<?php echo $biaya_konsumsi; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_biaya_alat" class="control-label col-sm-2">Biaya Alat</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_biaya_alat" id="txt_biaya_alat" class="form-control" placeholder="Biaya Alat" value="<?php echo $biaya_alat; ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" name="btn_save" value="<?php echo $val_btn; ?>">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#txt_date').datepicker({
        autoclose: true,
        changeMonth: true,
		changeYear: true,
        format: "yyyy-mm-dd"
    });

    $("#opt_bidang").change(function(){
        var val_bidang = $("#opt_bidang").val();
        $.ajax({
            url: "<?php echo site_url($modul."/ajax_trainer"); ?>",
            method: "post",
            data: {id_bidang: val_bidang},
            success: function(data) {
                $("#opt_user").html(data);
            }
        });
    });

    $("#opt_user").change(function(){
        var val_bidang = $("#opt_bidang").val();
        var val_user = $("#opt_user").val();
        $.ajax({
            url: "<?php echo site_url($modul."/ajax_course"); ?>",
            method: "post",
            data: {id_bidang: val_bidang, id_user: val_user},
            success: function(data) {
                $("#opt_course").html(data);
            }
        });
    });
});
</script>
