<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Tambah Peserta</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="<?php echo site_url($url); ?>" method="post">
            <input type="hidden" name="txt_id_course" value="<?php echo $id_course; ?>">
            <div class="form-group">
                <label for="txt_nama" class="control-label col-sm-2">Nama</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_nama" id="txt_nama" placeholder="Nama" class="form-control" autofocus="autofocus" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_email" class="control-label col-sm-2">Email</label>
                <div class="col-sm-10">
                    <input type="email" name="txt_email" id="txt_email" placeholder="Email" class="form-control" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_hp" class="control-label col-sm-2">HP</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_hp" id="txt_hp" placeholder="HP" class="form-control" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" name="btn_save" value="save">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#txt_date').datepicker({
        autoclose: true,
        changeMonth: true,
		changeYear: true,
        format: "yyyy-mm-dd"
    });

    $("#opt_bidang").change(function(){
        var val_bidang = $("#opt_bidang").val();
        $.ajax({
            url: "<?php echo site_url($modul."/ajax_trainer"); ?>",
            method: "post",
            data: {id_bidang: val_bidang},
            success: function(data) {
                $("#opt_user").html(data);
            }
        });
    });

    $("#opt_user").change(function(){
        var val_bidang = $("#opt_bidang").val();
        var val_user = $("#opt_user").val();
        $.ajax({
            url: "<?php echo site_url($modul."/ajax_course"); ?>",
            method: "post",
            data: {id_bidang: val_bidang, id_user: val_user},
            success: function(data) {
                $("#opt_course").html(data);
            }
        });
    });
});
</script>
