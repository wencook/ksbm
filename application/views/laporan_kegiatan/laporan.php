<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Laporan kegiatan</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-2">
                Nama Masjid
            </div>
            <div class="col-md-10">
                <?php echo $row->NAMA_MASJID." ".$row->PROVINSI_MASJID." ".$row->KABUPATEN_MASJID; ?>
            </div>
            <div class="col-md-2">
                Kursus
            </div>
            <div class="col-md-10">
                <?php echo $row->TITLE; ?>
            </div>
            <div class="col-md-2">
                Nama Instruktur
            </div>
            <div class="col-md-10">
                <?php echo $row->NAMA_INSTRUCTOR; ?>
            </div>
            <div class="col-md-12">
                Peserta
            </div>
        </div>
        <table class="table table-bordered table-striped" id="table-peserta">
            <thead>
                <tr>
                    <td>Nama</td>
                    <td>Email</td>
                    <td>Presence</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($res_peserta)
                {
                    foreach ($res_peserta as $v)
                    {
                        $wp["COURSE_PLAN_ID"] = $row->ID;
                        $wp["COURSE_TRAINEE_ID"] = $v->ID;
                        $row_wp = $this->tm->get_course_presence($wp)->row();
                        ?>
                        <tr>
                            <td><?php echo $v->NAMA; ?></td>
                            <td><?php echo $v->EMAIL; ?></td>
                            <td><?php echo $row_wp->PRESENCE; ?></td>
                            <td><a href="<?php echo site_url($url."/1/".$v->ID); ?>" class="btn btn-xs btn-primary">Hadir</a>&nbsp;<a href="<?php echo site_url($url."/0/".$v->ID); ?>" class="btn btn-xs btn-primary">Tidak Hadir</a></td>
                        </tr>
                        <?php
                    }
                }
                else
                {
                    echo "<tr><td colspan=\"3\">Belum Ada Peserta</td></tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="row" style="margin-top: 10px;">
            <?php
            if ($res_expense)
            {
                $total_biaya = 0;
                foreach ($res_expense as $ve)
                {
                    $total_biaya = $total_biaya + $ve->BIAYA_KELUAR;
                    ?>
                    <!-- <div class="row"> -->
                        <div class="col-md-2">
                            <?php echo $ve->KEPERLUAN; ?>
                        </div>
                        <div class="col-md-10">
                            <?php echo number_format($ve->BIAYA_KELUAR, 0, ",", "."); ?>
                        </div>
                    <!-- </div> -->
                    <?php
                }
                ?>
                <div class="col-md-2">
                    Total
                </div>
                <!-- <div class="col-md-offset-2 col-md-10"> -->
                <div class="col-md-10">
                    <?php echo number_format($total_biaya, 0, ",", "."); ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
