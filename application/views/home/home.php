<!-- Full Width Column -->
<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <!-- <section class="content-header">
            <h1>Top Navigation <small>Example 2.0</small></h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
            </ol>
        </section> -->

        <!-- Main content -->
        <section class="content">

            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">KSBM</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <form class="form-horizontal" method="post" action="<?php echo site_url($modul); ?>">
                        <div class="form-group">
                            <label for="opt_provinsi" class="col-sm-2 control-label">Provinsi</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="opt_provinsi" id="opt_provinsi">
                                    <option value="">Pilih Provinsi</option>
                                    <?php
                                    if ($res_provinsi)
                                    {
                                        foreach ($res_provinsi as $val)
                                        {
                                            ?>
                                            <option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="opt_kabupaten" class="col-sm-2 control-label">Kabupaten</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="opt_kabupaten" id="opt_kabupaten">
                                    <option value="">Pilih Kabupaten</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="opt_kecamatan" class="col-sm-2 control-label">Kecamatan</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="opt_kecamatan" id="opt_kecamatan">
                                    <option value="">Pilih Kecamatan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="opt_kelurahan" class="col-sm-2 control-label">Kelurahan / Desa</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="opt_kelurahan" id="opt_kelurahan">
                                    <option value="">Pilih Kelurahan/Desa</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="opt_bidang" class="col-sm-2 control-label">Bidang</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="opt_bidang" id="opt_bidang">
                                    <option value="">Pilih Bidang</option>
                                    <?php
                                    foreach ($res_keahlian as $vkeahlian)
                                    {
                                        ?>
                                        <option value="<?php echo $vkeahlian->ID; ?>"><?php echo $vkeahlian->KEAHLIAN; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" name="btn_filter" value="filter">Filter</button>
                            </div>
                        </div>
                    </form>

                    <table class="table table-bordered table-striped table-hover" style="margin-top: 10px;" id="tbl_kegiatan">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Lokasi</th>
                                <th>Nama Pelatihan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($res_kegiatan as $vk)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $vk->DATE; ?></td>
                                    <td><?php echo $vk->NAMA; ?></td>
                                    <td><?php echo $vk->TITLE; ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.container -->
</div>