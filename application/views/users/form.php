<?php
$user_id = isset($res_users) ? $res_users->user_id : "";
$user_email = isset($res_users) ? strtolower($res_users->user_email) : "";

$btn_value = isset($res_users) ? "btn_ubah" : "btn_simpan";

$konfirmasi = $btn_value === "btn_ubah" ? "onClick=\"return confirm('Apakah Anda Yakin?')\"" : "";

$req_password = isset($res_users) ? "" : "required=\"required\"";
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Form User</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" method="post" action="<?php echo base_url($modul."/save"); ?>">
        <div class="box-body">
            <div class="form-group">
                <label for="user_email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="user_email" id="user_email" placeholder="Email" value="<?php echo $user_email; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="user_password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="password"class="form-control" name="user_password" id="user_password" placeholder="Password" <?php echo $req_password; ?>>
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <input type="hidden" value="<?php echo $user_id; ?>" name="user_id">
            <button type="submit" class="btn btn-info btn-flat" name="btn_save" value="<?php echo $btn_value; ?>" id="<?php echo $btn_value; ?>" <?php echo $konfirmasi; ?>>Simpan</button>
            <button type="button" name="btn_cancel" id="btn_cancel" class="btn btn-default pull-right btn-flat">Cancel</button>
        </div><!-- /.box-footer -->
    </form>
</div><!-- /.box -->

<script type="text/javascript">
$("#btn_cancel").click(function(){
    window.location.replace("<?php echo base_url($modul); ?>");
});
$(document).ready(function() {
    $("#user_email").inputmask({alias: "email"});
 });
</script>
