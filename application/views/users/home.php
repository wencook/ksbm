<!-- TABLE: LATEST ORDERS -->
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Table User</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		</div>
	</div><!-- /.box-header -->
	<div class="box-body">
		<?php
		if ($this->session->flashdata("alert"))
		{
			?>
			<div class="clearfix">
				<div class="alert alert-<?php echo $this->session->flashdata("alert"); ?> alert-dismissible" id="alert-<?php echo $this->session->flashdata("alert"); ?>">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					Data <b><?php echo strtoupper($this->session->flashdata("keterangan")); ?></b> berhasil di<?php echo $this->session->flashdata("action"); ?>
				</div>
			</div>
			<?php
		}
		?>
		<div class="clearfix">
			<a href="<?php echo base_url($modul.'/form_new'); ?>" class="btn btn-sm bg-orange-active btn-flat pull-left"><i class="glyphicon glyphicon-plus"></i> Tambah User</a>
		</div>
		<div class="table-responsive">
			<table id="<?php echo $modul; ?>" class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Email</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($res_user as $val_user)
					{
						?>
						<tr>
							<td><?php echo $val_user->user_email; ?></td>
							<td>
								<div class="text-center">
									<a href="users/form_edit?id=<?php echo $val_user->user_id; ?>" class="btn btn-xs btn-flat btn-info"><i class="glyphicon glyphicon-edit"></i></a>
									<a href="users/delete_data?id=<?php echo $val_user->user_id; ?>" class="btn btn-xs btn-flat btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
								</div>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div><!-- /.table-responsive -->
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
	<a href="<?php echo base_url($modul.'/form_new'); ?>" class="btn btn-sm bg-orange-active btn-flat pull-left"><i class="glyphicon glyphicon-plus"></i> Tambah User</a>
	</div><!-- /.box-footer -->
</div><!-- /.box -->
