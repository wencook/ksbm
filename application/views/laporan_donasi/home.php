<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Laporan Donasi</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="<?php echo site_url($modul."/save_data"); ?>" method="post">
            <div class="form-group">
                <label for="txt_no_account" class="control-label col-sm-2">No. Akun</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_no_account" id="txt_no_account" placeholder="No. Akun" class="form-control" autofocus="autofocus" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_nama" class="control-label col-sm-2">Nama</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_nama" id="txt_nama" placeholder="Nama" class="form-control" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_jumlah" class="control-label col-sm-2">Jumlah</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_jumlah" id="txt_jumlah" placeholder="Jumlah" class="form-control" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" name="btn_save" value="save">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
