<?php
$id = isset($r_mosque) ? $r_mosque->ID : "";
$nama = isset($r_mosque) ? $r_mosque->NAMA : "";
$alamat = isset($r_mosque) ? $r_mosque->ALAMAT : "";
$dkm_ketua = isset($r_mosque) ? $r_mosque->DKM_KETUA : "";
$val_btn = isset($r_mosque) ? "update" : "save";
?>

<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Daftar Masjid</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="<?php echo site_url($modul."/".$url); ?>" method="post">
            <input type="hidden" name="txt_id" value="<?php echo $id; ?>">
            <div class="form-group">
                <label for="txt_nama" class="control-label col-sm-2">Nama</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_nama" id="txt_nama" class="form-control" placeholder="Nama" value="<?php echo $nama; ?>" autofocus="autofocus">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_nama" class="control-label col-sm-2">Alamat</label>
                <div class="col-sm-10">
                    <textarea name="txt_alamat" id="txt_alamat" class="form-control" rows="3" cols="80"><?php echo $alamat; ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="txt_dkm_ketua" class="control-label col-sm-2">Ketua DKM</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_dkm_ketua" id="txt_dkm_ketua" class="form-control" placeholder="Ketua DKM" value="<?php echo $dkm_ketua; ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" name="btn_save" value="<?php echo $val_btn; ?>">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
