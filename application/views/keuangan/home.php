<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Daftar Masjid</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <?php
        if ($this->uri->segment(3) == 1)
        {
            echo "Data telah diapprove";
        }
        elseif ($this->uri->segment(3) == 0)
        {
            echo "Data gagal diapprove";
        }
        ?>
        <table class="table table-bordered table-striped table-hover" id="table-<?php echo $modul; ?>">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">No. Akun</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Jumlah</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    var table_dt = $("#table-<?php echo $modul; ?>").DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url($modul.'/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [ 0 ], //first column
                "orderable": false, //set not orderable
            },
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            }
        ]

    });
});

function reload_table()
{
    table_dt.ajax.reload(null,false); //reload datatable ajax
}
</script>
