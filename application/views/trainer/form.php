<?php
$id = isset($r_trainer) ? $r_trainer->ID : "";
$instructor = isset($r_trainer) ? $r_trainer->INSTRUCTOR : $this->session->userdata('userEmail');
$bidang = isset($r_trainer) ? $r_trainer->BIDANG : "";
$title = isset($r_trainer) ? $r_trainer->TITLE : "";
$description = isset($r_trainer) ? $r_trainer->DESCRIPTION : "";
$keahlian1 = isset($r_trainer) ? $r_trainer->KEAHLIAN1 : "";
$keahlian2 = isset($r_trainer) ? $r_trainer->KEAHLIAN2 : "";
$keahlian3 = isset($r_trainer) ? $r_trainer->KEAHLIAN3 : "";
$keahlian4 = isset($r_trainer) ? $r_trainer->KEAHLIAN4 : "";
$keahlian5 = isset($r_trainer) ? $r_trainer->KEAHLIAN5 : "";
$keahlian6 = isset($r_trainer) ? $r_trainer->KEAHLIAN6 : "";
$keahlian7 = isset($r_trainer) ? $r_trainer->KEAHLIAN7 : "";
$keahlian8 = isset($r_trainer) ? $r_trainer->KEAHLIAN8 : "";
$keahlian9 = isset($r_trainer) ? $r_trainer->KEAHLIAN9 : "";
$keahlian10 = isset($r_trainer) ? $r_trainer->KEAHLIAN10 : "";
$val_btn = isset($r_trainer) ? "update" : "save";
?>

<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Daftar Masjid</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="<?php echo site_url($modul."/".$url); ?>" method="post">
            <input type="hidden" name="txt_id" value="<?php echo $id; ?>">
            <div class="form-group">
                <label for="txt_instructor" class="control-label col-sm-2">Instructor</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_instructor" id="txt_instructor" class="form-control" placeholder="Instructor" value="<?php echo $instructor; ?>" readonly="readonly">
                </div>
            </div>

            <div class="form-group">
                <label for="opt_bidang" class="control-label col-sm-2">Bidang</label>
                <div class="col-sm-10">
                    <select class="form-control" name="opt_bidang" id="opt_bidang" autofocus="autofocus">
                        <option value="">Pilih Bidang</option>
                        <?php
                        if ($res_keahlian)
                        {
                            foreach ($res_keahlian as $val)
                            {
                                $sel_keahlian = $val->ID == $bidang ? "selected=\"selected\"" : "";
                                ?>
                                <option value="<?php echo $val->ID; ?>" <?php echo $sel_keahlian; ?>><?php echo $val->KEAHLIAN; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="txt_title" class="control-label col-sm-2">Title</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_title" id="txt_title" class="form-control" placeholder="Title" value="<?php echo $title; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_description" class="control-label col-sm-2">Description</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_description" id="txt_description" class="form-control" placeholder="Description" value="<?php echo $description; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="txt_keahlian1" class="control-label col-sm-2">Keahlian Yang Diajarkan</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_keahlian1" id="txt_keahlian1" class="form-control" placeholder="Keahlian Yang Diajarkan 1" value="<?php echo $keahlian1; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian2" id="txt_keahlian2" class="form-control" placeholder="Keahlian Yang Diajarkan 2" value="<?php echo $keahlian2; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian3" id="txt_keahlian3" class="form-control" placeholder="Keahlian Yang Diajarkan 3" value="<?php echo $keahlian3; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian4" id="txt_keahlian4" class="form-control" placeholder="Keahlian Yang Diajarkan 4" value="<?php echo $keahlian4; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian5" id="txt_keahlian5" class="form-control" placeholder="Keahlian Yang Diajarkan 5" value="<?php echo $keahlian5; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian6" id="txt_keahlian6" class="form-control" placeholder="Keahlian Yang Diajarkan 6" value="<?php echo $keahlian6; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian7" id="txt_keahlian7" class="form-control" placeholder="Keahlian Yang Diajarkan 7" value="<?php echo $keahlian7; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian8" id="txt_keahlian8" class="form-control" placeholder="Keahlian Yang Diajarkan 8" value="<?php echo $keahlian8; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian9" id="txt_keahlian9" class="form-control" placeholder="Keahlian Yang Diajarkan 9" value="<?php echo $keahlian9; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="text" name="txt_keahlian10" id="txt_keahlian10" class="form-control" placeholder="Keahlian Yang Diajarkan 10" value="<?php echo $keahlian10; ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" name="btn_save" value="<?php echo $val_btn; ?>">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
