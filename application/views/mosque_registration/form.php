<?php
$id = isset($r_mosque) ? $r_mosque->ID : "";
$nama = isset($r_mosque) ? $r_mosque->NAMA : "";
$provinsi = isset($r_mosque) ? $r_mosque->PROVINSI : "";
$kabupaten = isset($r_mosque) ? $r_mosque->KABUPATEN : "";
$kecamatan = isset($r_mosque) ? $r_mosque->KECAMATAN : "";
$kelurahan = isset($r_mosque) ? $r_mosque->KELURAHAN : "";
$latitude = isset($r_mosque) ? $r_mosque->LATITUDE : "-3.4619199";
$longitude = isset($r_mosque) ? $r_mosque->LONGITUDE : "113.7607782";
$alamat = isset($r_mosque) ? $r_mosque->ALAMAT : "";
$dkm_ketua = isset($r_mosque) ? $r_mosque->DKM_KETUA : "";
$val_btn = isset($r_mosque) ? "update" : "save";
?>

<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Daftar Masjid</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="<?php echo site_url($modul."/".$url); ?>" method="post">
            <input type="hidden" name="txt_id" value="<?php echo $id; ?>">
            <div class="form-group">
                <label for="txt_nama" class="control-label col-sm-2">Nama</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_nama" id="txt_nama" class="form-control" placeholder="Nama" value="<?php echo $nama; ?>" autofocus="autofocus">
                </div>
            </div>

            <div class="row form-group">
                <label for="provinsi" class="control-label col-sm-2">Provinsi</label>
                <div class="col-sm-10">
                    <select id="provinsi" class="form-control" name="provinsi">
                        <option value="">-- Pilih Provinsi --</option>
                        <?php foreach($res_provinsi as $prov): ?>
                            <option value="<?= $prov['id'] ?>" data-latitude="<?= $prov['latitude'] ?>" data-longitude="<?= $prov['longitude'] ?>"><?= $prov['name'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <label for="kabupaten" class="control-label col-sm-2">Kabupaten</label>
                <div class="col-sm-10">
                    <select id="kabupaten" class="form-control" name="kabupaten">
                        <option value="">-- Pilih Kabupaten --</option>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <label for="kecamatan" class="control-label col-sm-2">Kecamatan</label>
                <div class="col-sm-10">
                    <select id="kecamatan" class="form-control" name="kecamatan">
                        <option value="">-- Pilih Kecamatan --</option>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <label for="kelurahan" class="control-label col-sm-2">Kelurahan</label>
                <div class="col-sm-10">
                    <select id="kelurahan" class="form-control" name="kelurahan">
                        <option value="">-- Pilih Kelurahan --</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div id="maps" class="col-md-12" style="height:400px;"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="row">
                        <div class="col-md-6">
                            Latitude : <span id="latitude"></span>
                            <input type="hidden" name="latitude" class="latitude" />
                        </div>
                        <div class="col-md-6">
                            Longitude : <span id="longitude"></span>
                            <input type="hidden" name="longitude" class="longitude" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="alamat" class="control-label col-sm-2">Alamat</label>
                <div class="col-sm-10">
                    <textarea name="alamat" id="alamat" class="form-control" rows="3" cols="80"><?php echo $alamat; ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="txt_dkm_ketua" class="control-label col-sm-2">Ketua DKM</label>
                <div class="col-sm-10">
                    <input type="text" name="txt_dkm_ketua" id="txt_dkm_ketua" class="form-control" placeholder="Ketua DKM" value="<?php echo $dkm_ketua; ?>">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" name="btn_save" value="<?php echo $val_btn; ?>">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<?php echo base_url('assets/js/front.js') ?>"></script>
<script src="<?php echo base_url('assets/js/maps.js') ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFRf2SEcuWSs5h8vKNX4u3bfM5oCsWtik"></script>
<script>
  $(document).ready(function(){

    function initialize(lati, longi, zoom_map) {
        var posisi = {lat: lati, lng: longi};
        var map_canvas = document.getElementById('maps');
        var map_options = {
            center: new google.maps.LatLng(lati, longi),
            zoom: zoom_map,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(map_canvas, map_options)
    }

    google.maps.event.addDomListener(window, 'load', initialize(-3.4619199, 113.7607782, 5));

    function defaultProvinsi()
    {
        $('#kabupaten').html('<option value="">-- Pilih Kabupaten --</option>');
        $('#kecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
        $('#kelurahan').html('<option value="">-- Pilih Kelurahan --</option>');
        $("#alamat").val("");
        $("#latitude").html("");
        $("#longitude").html("");
        $(".latitude").val("");
        $(".longitude").val("");
    }

    function defaultKabupaten()
    {
        $('#kecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
        $('#kelurahan').html('<option value="">-- Pilih Kelurahan --</option>');
        $("#alamat").val("");
        $("#latitude").html("");
        $("#longitude").html("");
        $(".latitude").val("");
        $(".longitude").val("");
    }

    function defaultKecamatan()
    {
        $('#kelurahan').html('<option value="">-- Pilih Kelurahan --</option>');
        $("#alamat").val("");
        $("#latitude").html("");
        $("#longitude").html("");
        $(".latitude").val("");
        $(".longitude").val("");
    }

    var baseURL = '<?php echo base_url() ?>';

    $('#provinsi').on('change', function(){

        defaultProvinsi();

        var value = $(this).val();

        if(value == ""){

            defaultProvinsi();

        }else{

            latmarker = $('option:selected', this).data('latitude');
            longimarker = $('option:selected', this).data('longitude');
            zoomarker = 10;
            google.maps.event.addDomListener(window, 'load', initialize(latmarker, longimarker, zoomarker));

            $.ajax({
                url: baseURL+'mosque_registration/kabupaten/'+value,
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, item) {
                        $('#kabupaten').append('<option value="'+item.id+'" data-latitude="'+item.latitude+'" data-longitude="'+item.longitude+'">'+item.name+'</option>');
                    })
                }
            });

        }
    });


    //kabupaten on change
    $('#kabupaten').on('change', function(){

      defaultKabupaten();

      var value = $(this).val();

      if(value == ""){

        defaultKabupaten();

      }else{
        latmarker = $('option:selected', this).data('latitude');
        longimarker = $('option:selected', this).data('longitude');
        zoomarker = 13;
        google.maps.event.addDomListener(window, 'load', initialize(latmarker, longimarker, zoomarker));

        $.ajax({
            url: baseURL+'mosque_registration/kecamatan/'+value,
            dataType: 'json',
            success: function(data) {
                $.each(data, function(i, item) {
                    $('#kecamatan').append('<option value="'+item.id+'" data-latitude="'+item.latitude+'" data-longitude="'+item.longitude+'">'+item.name+'</option>');
                })
            }
        });

      }
    });


    //Kecamatan on change
    $('#kecamatan').on('change', function(){

      defaultKecamatan();

      var value = $(this).val();

      if(value == ""){

        defaultKecamatan();

      }else{

        latmarker = $('option:selected', this).data('latitude');
        longimarker = $('option:selected', this).data('longitude');
        zoomarker = 15;
        google.maps.event.addDomListener(window, 'load', initialize(latmarker, longimarker, zoomarker));

        $.ajax({
            url: baseURL+'mosque_registration/kelurahan/'+value,
            dataType: 'json',
            success: function(data) {
                $.each(data, function(i, item) {
                    $('#kelurahan').append('<option value="'+item.id+'" data-latitude="'+item.latitude+'" data-longitude="'+item.longitude+'">'+item.name+'</option>');
                })
            }
        });

      }
    });


    //Kelurahan on change
    $('#kelurahan').on('change', function(){

      var value = $(this).val();

      if(value == ""){


      }else{
        latmarker = $('option:selected', this).data('latitude');
        longimarker = $('option:selected', this).data('longitude');
        zoomarker = 17;
        showWithMarker(latmarker, longimarker, zoomarker);

      }
    });

});
</script>
