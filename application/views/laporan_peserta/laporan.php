<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Laporan kegiatan</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-2">
                Nama Masjid
            </div>
            <div class="col-md-10">
                <?php echo $row->NAMA_MASJID." ".$row->PROVINSI_MASJID." ".$row->KABUPATEN_MASJID; ?>
            </div>
            <div class="col-md-2">
                Kursus
            </div>
            <div class="col-md-10">
                <?php echo $row->TITLE; ?>
            </div>
            <div class="col-md-2">
                Nama Instruktur
            </div>
            <div class="col-md-10">
                <?php echo $row->NAMA_INSTRUCTOR; ?>
            </div>
        </div>
        <form action="<?php echo site_url($url); ?>" method="post">
            <input type="hidden" name="txt_id_course" value="<?php echo $row->ID; ?>">
            <table class="table table-bordered table-striped" id="table-materi">
                <thead>
                    <tr>
                        <td>Materi</td>
                        <td>Penguasaan Materi</td>
                        <td>Dipahami</td>
                        <td>Bisa Praktikum</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $arr_materi = array($row->KEAHLIAN1,$row->KEAHLIAN2,$row->KEAHLIAN3,$row->KEAHLIAN4,$row->KEAHLIAN5,$row->KEAHLIAN6,$row->KEAHLIAN7,$row->KEAHLIAN8,$row->KEAHLIAN9,$row->KEAHLIAN10);
                    foreach ($arr_materi as $key => $value)
                    {
                        if ($value != NULL)
                        {
                            ?>
                            <tr>
                                <input type="hidden" name="txt_id_catalogue[]" value="<?php echo $row->ID_COURSE_CATALOGUE; ?>">
                                <td><?php echo $value; ?></td>
                                <td>
                                    <select class="form-control" name="opt_penguasaan_materi[]">
                                        <option value="1">Tidak Bagus</option>
                                        <option value="2">Kurang Bagus</option>
                                        <option value="3">Bagus</option>
                                        <option value="4">Sangat Bagus</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" name="opt_dipahami[]">
                                        <option value="1">Tidak Bagus</option>
                                        <option value="2">Kurang Bagus</option>
                                        <option value="3">Bagus</option>
                                        <option value="4">Sangat Bagus</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" name="opt_praktikum[]">
                                        <option value="1">Tidak Bagus</option>
                                        <option value="2">Kurang Bagus</option>
                                        <option value="3">Bagus</option>
                                        <option value="4">Sangat Bagus</option>
                                    </select>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <button type="submit" name="btn_save" value="save" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
