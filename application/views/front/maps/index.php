<section class="section">
  <div class="container" style="padding-top:20px;">

    <?php $this->load->view('partials/alert'); ?>

    <div class="row">
      <div class="col-sm-12 post">
        <div class="col-md-2">
          <div class="row form-group">
            <select id="provinsi" class="form-control col-md-12" name="provinsi">
              <option value="">-- Pilih Provinsi --</option>
              <?php foreach($data as $prov): ?>
              <option value="<?= $prov['id'] ?>" data-latitude="<?= $prov['latitude'] ?>" data-longitude="<?= $prov['longitude'] ?>"><?= $prov['name'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="row form-group">
            <select id="kabupaten" class="form-control" name="kabupaten">
              <option value="">-- Pilih Kabupaten --</option>
            </select>
          </div>
          <div class="row form-group">
            <select id="kecamatan" class="form-control" name="kecamatan">
              <option value="">-- Pilih Kecamatan --</option>
            </select>
          </div>
          <div class="row form-group">
            <select id="kelurahan" class="form-control" name="kelurahan">
              <option value="">-- Pilih Kelurahan --</option>
            </select>
          </div>
          
        </div>

        <div class="col-md-9">
          <?php echo form_open_multipart('maps/store'); ?>
          <div id="maps" class="col-md-12" style="height:400px;">
          </div>&nbsp;
          <div class="col-md-12">
            <label class="row">ALAMAT</label>
            <div class="row form-group">
              <input type="text" id="alamat" readonly="true" class="form-control" name="alamat"></input>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row col-md-6">
              Latitude : <span id="latitude"></span>
              <input type="hidden" name="latitude" class="latitude" />
            </div>
            <div class="row col-md-6">
              Longitude : <span id="longitude"></span>
              <input type="hidden" name="longitude" class="longitude" />
            </div>
          </div></br></br></br>&nbsp;
          <div class="col-md-12">
            <label class="row">MASALAH</label>
            <div class="row form-group">
              <textarea class="form-control" name="masalah"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row form-group">
              <button type="submit" class="btn btn-primary">SUBMIT</button>
            </div>
          </div>
          <?php echo form_close(); ?>
        </div>

      </div>
    </div>
  </div>
</section>


<script src="<?php echo base_url('assets/js/front.js') ?>"></script>
<script src="<?php echo base_url('assets/js/maps.js') ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFRf2SEcuWSs5h8vKNX4u3bfM5oCsWtik"></script>
<script>
  $(document).ready(function(){
    
    function initialize(lati, longi, zoom_map) {
      var posisi = {lat: lati, lng: longi};
      var map_canvas = document.getElementById('maps');
      var map_options = {
        center: new google.maps.LatLng(lati, longi),
        zoom: zoom_map,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      var map = new google.maps.Map(map_canvas, map_options)
    }

    google.maps.event.addDomListener(window, 'load', initialize(-3.4619199, 113.7607782, 5));

    function defaultProvinsi()
    {
        $('#kabupaten').html('<option value="">-- Pilih Kabupaten --</option>');
        $('#kecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
        $('#kelurahan').html('<option value="">-- Pilih Kelurahan --</option>');
        $("#alamat").val("");
        $("#latitude").html("");
        $("#longitude").html("");
        $(".latitude").val("");
        $(".longitude").val("");
    }

    function defaultKabupaten()
    {
      $('#kecamatan').html('<option value="">-- Pilih Kecamatan --</option>');
      $('#kelurahan').html('<option value="">-- Pilih Kelurahan --</option>');
      $("#alamat").val("");
      $("#latitude").html("");
      $("#longitude").html("");
      $(".latitude").val("");
      $(".longitude").val("");
    }

    function defaultKecamatan()
    {
      $('#kelurahan').html('<option value="">-- Pilih Kelurahan --</option>');
      $("#alamat").val("");
      $("#latitude").html("");
      $("#longitude").html("");
      $(".latitude").val("");
      $(".longitude").val("");
    }

    var baseURL = '<?php echo base_url() ?>';

    $('#provinsi').on('change', function(){

      defaultProvinsi();

      var value = $(this).val();

      if(value == ""){

        defaultProvinsi();

      }else{

        latmarker = $('option:selected', this).data('latitude');
        longimarker = $('option:selected', this).data('longitude');
        zoomarker = 10;
        google.maps.event.addDomListener(window, 'load', initialize(latmarker, longimarker, zoomarker));

        $.ajax({
            url: baseURL+'maps/kabupaten/'+value,
            dataType: 'json',
            success: function(data) {     
                $.each(data, function(i, item) {
                    $('#kabupaten').append('<option value="'+item.id+'" data-latitude="'+item.latitude+'" data-longitude="'+item.longitude+'">'+item.name+'</option>');
                })
            }
        });

      }
    });


    //kabupaten on change
    $('#kabupaten').on('change', function(){

      defaultKabupaten();

      var value = $(this).val();

      if(value == ""){

        defaultKabupaten();

      }else{
        latmarker = $('option:selected', this).data('latitude');
        longimarker = $('option:selected', this).data('longitude');
        zoomarker = 13;
        google.maps.event.addDomListener(window, 'load', initialize(latmarker, longimarker, zoomarker));

        $.ajax({
            url: baseURL+'maps/kecamatan/'+value,
            dataType: 'json',
            success: function(data) {     
                $.each(data, function(i, item) {
                    $('#kecamatan').append('<option value="'+item.id+'" data-latitude="'+item.latitude+'" data-longitude="'+item.longitude+'">'+item.name+'</option>');
                })
            }
        });

      }
    });


    //Kecamatan on change
    $('#kecamatan').on('change', function(){

      defaultKecamatan();

      var value = $(this).val();

      if(value == ""){

        defaultKecamatan();

      }else{
        
        latmarker = $('option:selected', this).data('latitude');
        longimarker = $('option:selected', this).data('longitude');
        zoomarker = 15;
        google.maps.event.addDomListener(window, 'load', initialize(latmarker, longimarker, zoomarker));

        $.ajax({
            url: baseURL+'maps/kelurahan/'+value,
            dataType: 'json',
            success: function(data) {     
                $.each(data, function(i, item) {
                    $('#kelurahan').append('<option value="'+item.id+'" data-latitude="'+item.latitude+'" data-longitude="'+item.longitude+'">'+item.name+'</option>');
                })
            }
        });

      }
    });


    //Kelurahan on change
    $('#kelurahan').on('change', function(){

      var value = $(this).val();

      if(value == ""){


      }else{
        latmarker = $('option:selected', this).data('latitude');
        longimarker = $('option:selected', this).data('longitude');
        zoomarker = 17;
        showWithMarker(latmarker, longimarker, zoomarker);

      }
    });

});
</script>