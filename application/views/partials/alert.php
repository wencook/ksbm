<?php if ($this->session->has_userdata('success')): ?>
  <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>

    <?php echo $this->session->flashdata('success'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->has_userdata('info')): ?>
  <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>

    <?php echo $this->session->flashdata('info'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->has_userdata('warning')): ?>
  <div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>

    <?php echo $this->session->flashdata('warning'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->has_userdata('error')): ?>
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>

    <?php echo $this->session->flashdata('error'); ?>
  </div>
<?php endif; ?>

<?php if ($this->session->has_userdata('errors')): ?>
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <ul style="padding-left: 15px;">
      <?php foreach ($this->session->flashdata('errors') as $key => $value): ?>
        <li><?php echo $value; ?></li>
      <?php endforeach ?>
    </ul>
  </div>
<?php endif; ?>