<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SI-Spp | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/font-awesome/css/font-awesome.min.css"); ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/ionicons/css/ionicons.min.css"); ?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.min.css"); ?>">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
        -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/skins/skin-blue.min.css"); ?>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="<?php echo base_url(); ?>"><b>CI Web</a>
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>
                <p class="login-box-msg">
                    <?php echo isset($alert) ? "<div class=\"callout callout-danger\">".$alert."</div>" : ""; ?>
                </p>
                <form action="<?php echo base_url('auth/validate_credential'); ?>" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="txt_username" id="txt_username" class="form-control" placeholder="Email" autofocus="autofocus" autocomplete="off" required="required">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="txt_password" id="txt_password" class="form-control" placeholder="Password" required="required">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" name="btn_login" id="btn_login" value="btn_login" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div><!-- /.col -->
                    </div>
                </form>

                <!-- <a href="#">I forgot my password</a><br> -->
                <a href="<?php echo base_url("user_registration"); ?>" class="text-center">Register</a>

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    </body>
</html>
