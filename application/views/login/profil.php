<?php
$user_id = isset($row_user) ? $row_user->user_id : "";
$user_email = isset($row_user) ? $row_user->user_email : "";
$btn_value = isset($row_user) ? "btn_ubah" : "btn_simpan";

$konfirmasi = $btn_value === "btn_ubah" ? "onClick=\"return confirm('Apakah Anda Yakin?')\"" : "";
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Form Profil</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <?php
        if ($this->session->flashdata("alert"))
        {
            ?>
            <div class="clearfix">
                <div class="alert alert-<?php echo $this->session->flashdata("alert"); ?> alert-dismissible" id="alert-<?php echo $this->session->flashdata("alert"); ?>">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Data <b><?php echo strtoupper($this->session->flashdata("keterangan")); ?></b> berhasil di<?php echo $this->session->flashdata("action"); ?>
                </div>
            </div>
            <?php
        }
        ?>
        <!-- form start -->
        <form class="form-horizontal" method="post" action="<?php echo base_url("auth/save_profil"); ?>">
            <div class="box-body">
                <div class="form-group">
                    <label for="user_email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="user_email" id="user_email" placeholder="Email" value="<?php echo $user_email; ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="user_password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Password">
                            </div>
                            <div class="col-sm-4">
                                <p class="help-block">Isi jika ingin merubah password</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <input type="hidden" value="<?php echo $user_id; ?>" name="user_id">
                <button type="submit" class="btn btn-info btn-flat" name="btn_save" value="<?php echo $btn_value; ?>" id="<?php echo $btn_value; ?>" <?php echo $konfirmasi; ?>>Simpan</button>
                <button type="button" name="btn_cancel" id="btn_cancel" class="btn btn-default pull-right btn-flat">Cancel</button>
            </div><!-- /.box-footer -->
        </form>
    </div>
</div><!-- /.box -->

<script type="text/javascript">
$(document).ready(function(){
    $("#alert-<?php echo $this->session->flashdata("alert"); ?>").fadeTo(5000,500).slideUp(500, function(){
        $("#alert-<?php echo $this->session->flashdata("alert"); ?>").slideUp(500);
    });
});

$("#btn_cancel").click(function(){
    window.location.replace("<?php echo base_url(); ?>");
});
</script>
