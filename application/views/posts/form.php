<?php
$post_id = isset($posts) ? $posts->post_id : "";
$title = isset($posts) ? ($posts->title) : "";
$content = isset($posts) ? ($posts->content) : "";
$created = isset($posts) ? ($posts->created) : "";

$btn_value = isset($res_users) ? "btn_ubah" : "btn_simpan";

$konfirmasi = $btn_value === "btn_ubah" ? "onClick=\"return confirm('Apakah Anda Yakin?')\"" : "";

?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Form User</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" method="post" action="<?php echo base_url($modul."/save"); ?>">
        <div class="box-body">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>">
                </div>
            </div>
            
            <div class="form-group">
				<label for="content" class="col-sm-2 control-label">Content</label>
				<div class="col-sm-10">
					<textarea class="form-control" id="content" name="content"><?php echo $content; ?></textarea>
				</div>
            </div>

            <div class="form-group">
                <label for="created" class="col-sm-2 control-label">Created</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="created" id="created" placeholder="Created" value="<?php echo $created; ?>">
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <input type="hidden" value="<?php echo $post_id; ?>" name="post_id">
            <button type="submit" class="btn btn-info btn-flat" name="btn_save" value="<?php echo $btn_value; ?>" id="<?php echo $btn_value; ?>" <?php echo $konfirmasi; ?>>Simpan</button>
            <button type="button" name="btn_cancel" id="btn_cancel" class="btn btn-default pull-right btn-flat">Cancel</button>
        </div><!-- /.box-footer -->
    </form>
</div><!-- /.box -->

<script type="text/javascript">
$("#btn_cancel").click(function(){
    window.location.replace("<?php echo base_url($modul); ?>");
});
$(document).ready(function() {
    $("#user_email").inputmask({alias: "email"});
 });
</script>
