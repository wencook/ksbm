<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Laporan kegiatan</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-2">
                Nama Masjid
            </div>
            <div class="col-md-10">
                <?php echo $row->NAMA_MASJID." ".$row->PROVINSI_MASJID." ".$row->KABUPATEN_MASJID; ?>
            </div>
            <div class="col-md-2">
                Kursus
            </div>
            <div class="col-md-10">
                <?php echo $row->TITLE; ?>
            </div>
            <div class="col-md-2">
                Nama Instruktur
            </div>
            <div class="col-md-10">
                <?php echo $row->NAMA_INSTRUCTOR; ?>
            </div>
            <div class="col-md-12">
                Peserta
            </div>
        </div>
        <form action="<?php echo site_url($url); ?>" method="post">
            <input type="hidden" name="txt_id_course" value="<?php echo $row->ID; ?>">
            <table class="table table-bordered table-striped" id="table-peserta">
                <thead>
                    <tr>
                        <td>Nama</td>
                        <td>Pemahaman</td>
                        <td>Praktikum</td>
                        <td>Minat</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($res_peserta)
                    {
                        foreach ($res_peserta as $v)
                        {
                            ?>
                            <tr>
                                <input type="hidden" name="txt_id_trainee[]" value="<?php echo $v->ID; ?>">
                                <td><?php echo $v->NAMA; ?></td>
                                <td>
                                    <select class="form-control" name="opt_pemahaman[]">
                                        <option value="1">Tidak Bagus</option>
                                        <option value="2">Kurang Bagus</option>
                                        <option value="3">Bagus</option>
                                        <option value="4">Sangat Bagus</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" name="opt_praktikum[]">
                                        <option value="1">Tidak Bagus</option>
                                        <option value="2">Kurang Bagus</option>
                                        <option value="3">Bagus</option>
                                        <option value="4">Sangat Bagus</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" name="opt_minat[]">
                                        <option value="1">Tidak Bagus</option>
                                        <option value="2">Kurang Bagus</option>
                                        <option value="3">Bagus</option>
                                        <option value="4">Sangat Bagus</option>
                                    </select>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    else
                    {
                        echo "<tr><td colspan=\"3\">Belum Ada Peserta</td></tr>";
                    }
                    ?>
                </tbody>
            </table>
            <button type="submit" name="btn_save" value="save" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
