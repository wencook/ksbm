<div class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Daftar Peserta Kursus</h3>
        <div class="box-tools pull-right">
            <?php echo isset($pr) ? $pr : ""; ?>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-2">
                Nama Masjid
            </div>
            <div class="col-md-10">
                <?php echo $row->NAMA_MASJID." ".$row->PROVINSI_MASJID." ".$row->KABUPATEN_MASJID; ?>
            </div>
            <div class="col-md-2">
                Kursus
            </div>
            <div class="col-md-10">
                <?php echo $row->TITLE; ?>
            </div>
            <div class="col-md-2">
                Nama Instruktur
            </div>
            <div class="col-md-10">
                <?php echo $row->NAMA_INSTRUCTOR; ?>
            </div>
            <div class="col-md-12">
                Peserta
            </div>
        </div>
        <table class="table table-bordered table-striped" id="table-peserta">
            <thead>
                <tr>
                    <td>Nama</td>
                    <td>Email</td>
                    <td>Status</td>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($res_peserta)
                {
                    foreach ($res_peserta as $v)
                    {
                        ?>
                        <tr>
                            <td><?php echo $v->NAMA; ?></td>
                            <td><?php echo $v->EMAIL; ?></td>
                            <td><?php echo $v->STATUS; ?></td>
                        </tr>
                        <?php
                    }
                }
                else
                {
                    echo "<tr><td colspan=\"3\">Belum Ada Peserta</td></tr>";
                }
                ?>
            </tbody>
        </table>
        <a href="<?php echo site_url($url); ?>" class="btn btn-primary" style="margin-top: 5px;">Add</a>
    </div>
</div>
