<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <p>Assalamu'alaykum</p>
        <p><?php echo $row->NAMA_PESERTA; ?>, Anda diundang bergerak ke pelatihan <?php echo $row->TITLE; ?> di Masjid <?php echo $row->NAMA_MASJID." ".$row->PROVINSI_MASJID." ".$row->KABUPATEN_MASJID; ?> yang akan diisi oleh instruktur <?php echo $row->NAMA_INSTRUCTOR; ?></p>
        <p>Klik link berikut jika anda bersedia untuk mengikuti pelatihan <a href="<?php echo base_url("course/aktivasi_peserta/".$row->ID_PESERTA); ?>">Daftar</a></p>
        <p>Wassalam</p>
    </body>
</html>
