<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Table User</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		</div>
	</div><!-- /.box-header -->
	<div class="box-body">

        <?php echo $error;?>
        <form action="<?php echo base_url($modul."/do_upload"); ?>" method="post" enctype="multipart/form-data" class="form-inline">
            <div class="form-group">
                <input type="file" name="userfile" size="20" />
            </div>
            <input type="submit" value="upload" class="btn btn-defult" />
        </form>

        <div class="row">
            <?php
            if (isset($res_gallery) && $res_gallery !== FALSE)
            {
                foreach ($res_gallery as $val_gallery)
				{
					?>
					<div class="col-sm-4 col-md-4">
		                <a href="#" class="thumbnail">
		                    <img class="img-responsive" src="<?php echo base_url("uploads/").$val_gallery->gallery_ket; ?>" alt="avatar">
		                </a>
		            </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
