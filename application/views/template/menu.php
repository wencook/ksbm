        <!-- Sidebar Menu -->
        <ul class="sidebar-menu nav">
            <li class="header">Main Menu</li>
            <!-- Optionally, you can add icons to the links -->
            <li id="mHome" class="active"><a href="<?php echo site_url("admin_home"); ?>"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
            <!-- <li id="musers"><a href="<?php echo site_url("users"); ?>">User</a></li>
            <li id="mgallery"><a href="<?php echo site_url("gallery"); ?>">Gallery</a></li>
            <li id="mposts"><a href="<?php echo site_url("posts"); ?>">Posts</a></li> -->
            <?php
            if ($this->session->userdata('item') == "")
            {
                ?>
                <?php
            }
            ?>
            <?php
            if ($this->session->userdata('userPeran')==0)
            {
                ?>
                <li id="mapproval"><a href="<?php echo site_url("approval"); ?>">Approval</a></li>
                <?php
            }
            else
            {
                ?>
                <li id="mmosque_registration"><a href="<?php echo site_url("mosque_registration"); ?>">Masjid</a></li>
                <li id="mtrainer"><a href="<?php echo site_url("trainer"); ?>">Trainer</a></li>
                <li id="mjadwal_pelatihan"><a href="<?php echo site_url("jadwal_pelatihan"); ?>">Jadwal Pelatihan</a></li>
                <li id="mlaporan_donasi"><a href="<?php echo site_url("laporan_donasi"); ?>">Laporan Donasi</a></li>
                <li id="mkeuangan"><a href="<?php echo site_url("keuangan"); ?>">Keuangan</a></li>
                <li id="mlaporan_keuangan"><a href="<?php echo site_url("laporan_keuangan"); ?>">LaporanKeuangan</a></li>
                <li id="mcourse"><a href="<?php echo site_url("course"); ?>">Course</a></li>
                <li id="mlaporan_kegiatan"><a href="<?php echo site_url("laporan_kegiatan"); ?>">Laporan Kegiatan</a></li>
                <li id="mlaporan_instructor"><a href="<?php echo site_url("laporan_instructor"); ?>">Laporan Instructor</a></li>
                <li id="mlaporan_peserta"><a href="<?php echo site_url("laporan_peserta"); ?>">Laporan Peserta</a></li>
                <li id="mmy_course"><a href="<?php echo site_url("my_course"); ?>">My Course</a></li>
                <?php
            }
            ?>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>
    <?php echo isset($ph) ? $ph : "Dashboard"; ?>
    <small><?php echo isset($pd) ? $pd : ""; ?></small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
</ol>
</section>

<!-- Main content -->
<section class="content">

<!-- Your Page Content Here -->

<script>
$(document).ready(function(){
    	<?php
    	if ($this->uri->segment(1) == $modul)
    	{
    		?>
            $(".nav").find(".active").removeClass("active");
    		$("#m<?php echo $modul; ?>").addClass("active");
    		<?php
    	}
    	else
    	{
    		?>
            $(".nav").find(".active").removeClass("active");
    		$("#mHome").addClass("active");
    		<?php
    	}
    	?>
});
</script>
