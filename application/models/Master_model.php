<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_data($return="result", $field, $table, $left_join=NULL, $where=NULL, $like=NULL, $order=NULL, $limit=NULL, $distinct=NULL)
    {
        $this->db->select($field);
        $this->db->from($table);

        if ($left_join !== NULL)
        {
            foreach ($join as $key_join => $val_join)
            {
                $this->db->join($key_join, $val_join, 'left');
            }
        }

        if ($where !== NULL)
        {
            foreach ($where as $key_where => $val_where)
            {
                $this->db->where($key_where, $val_where);
            }
        }

        if ($like !== NULL)
        {
            foreach ($like as $key_like => $val_like)
            {
                $this->db->like($key_like, $val_like);
            }
        }

        if ($order !== NULL)
        {
            foreach($order as $key_order => $val_order)
            {
                $this->db->order_by($key_order, $val_order);
            }
        }

        if ($limit !== NULL)
        {
            foreach ($limit as $key_limit => $val_limit)
            {
                $this->db->limit($key_limit, $val_limit);
            }
        }

        if ($distinct !== NULL)
        {
            $this->db->distinct();
        }

        $qry = $this->db->get();
        // echo $this->db->last_query();

        if ($return === "row")
        {
            return $qry->num_rows() > 0 ? $qry->row() : FALSE;
        }
        else
        {
            return $qry->num_rows() > 0 ? $qry->result() : FALSE;
        }
    }

    /*
     * @param string $table nama table
     * @param string $data array data yang akan diinput beserta nama field
     */
    function insert_data($table, $data)
    {
        // $sql = $this->db->set($data)->get_compiled_insert($table);
        // echo $sql;exit;
        return $this->db->insert($table, $data);
    }

    /*
     * @param string $table nama table
     * @param string $data field dan data yang akan diupdate
     * @param array $field field where
     * @param array $where data yang dicari
     */
    function update_data($table, $data, $where)
    {
        foreach ($where as $key => $row)
        {
            $this->db->where($key, $row);
        }
        $this->db->update($table, $data);
        // echo $this->db->last_query();
    }

    /*
     * @param string $table nama table
     * @param array $where data yang dicari
     */
    function delete_data($table, $where)
    {
        foreach ($where as $key => $value)
        {

            $this->db->where($key, $value);
        }
        return $this->db->delete($table);
    }
    /*
     * @param string $table nama table
     * @param array $like data yang dicari
     */
    function check_duplicate($table, $like)
    {
        foreach ($like as $key => $row)
        {
            $this->db->like($key, $row, "none");
        }

        $this->db->from($table);

        return $this->db->count_all_results();
    }

    function validate($username, $userpassword)
    {
        // $hashAndSalt = $this->get_password($username);
        // $password_hash = password_hash($userpassword, PASSWORD_BCRYPT, array("cost" => 12)); echo $password_hash;

        $this->db->select("user_id, user_email, user_password");
        $this->db->where('user_email', $username);
        $query = $this->db->get('users');
        // echo $this->db->last_query();exit;
        $row_user = $query->num_rows() > 0 ? $query->row() : FALSE;
        if ($row_user !== FALSE)
        {
            if (password_verify($userpassword,$row_user->user_password))
            {
                return $query->row();
                // return array("user_id"=>$row_user->user_id,"user_name"=>$row_user->user_name,"user_email"=>$row_user->user_email);
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }
}
