<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends MY_Model
{
	protected $table = 'ADMIN_USER_TBL';
    protected $pk = 'ID';

    function __construct()
    {
        parent::__construct();
    }

    public function get_user_by_id($id) {
		return $this->get_data("row","*", NULL, array($this->pk => $id));
	}

    public function validate($username, $userpassword)
    {
        // $hashAndSalt = $this->get_password($username);
        // $password_hash = password_hash($userpassword, PASSWORD_BCRYPT, array("cost" => 12)); echo $password_hash;

        $this->db->select("ID, EMAIL, PASSWORD, PERAN");
        $this->db->where('EMAIL', $username);
		$this->db->where('PASSWORD', $userpassword);
        $query = $this->db->get($this->table);
        // echo $this->db->last_query();exit;
        $row_user = $query->num_rows() > 0 ? $query->row() : FALSE;
        if ($row_user !== FALSE)
        {

        	// ekobs
        	return $query->row();
//             if (password_verify($userpassword,$row_user->user_password))
//             {
//                 return $query->row();
//                 // return array("user_id"=>$row_user->user_id,"user_name"=>$row_user->user_name,"user_email"=>$row_user->user_email);
//             }
//             else
//             {
//                 return FALSE;
//             }
        }
        else
        {
            return FALSE;
        }
    }

}
