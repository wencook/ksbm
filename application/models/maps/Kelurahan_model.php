<?php

class Kelurahan_model extends CI_Model
{
    private $table = 'kelurahan_tbl';

    public function findMuch($id, $columns = '*')
    {
        $this->db->select($columns);

        $this->db->where('kecamatanId', $id);

        $this->db->from($this->table);

        return $this->db->get()->result_array();
    }
}