<?php

class Kabupaten_model extends CI_Model
{
    private $table = 'kabupaten_tbl';

    public function findMuch($id, $columns = '*')
    {
        $this->db->select($columns);

        $this->db->where('provinsiId', $id);

        $this->db->from($this->table);

        return $this->db->get()->result_array();
    }
}