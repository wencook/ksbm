<?php

class Titikpantau_model extends CI_Model
{
    private $table = 'titik_pantau_tbl';

    public function create(array $data)
    {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }

}