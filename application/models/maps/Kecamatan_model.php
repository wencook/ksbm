<?php

class Kecamatan_model extends CI_Model
{
    private $table = 'kecamatan_tbl';

    public function findMuch($id, $columns = '*')
    {
        $this->db->select($columns);

        $this->db->where('kabupatenId', $id);

        $this->db->from($this->table);

        return $this->db->get()->result_array();
    }
}