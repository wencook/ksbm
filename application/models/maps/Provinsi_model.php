<?php

class Provinsi_model extends CI_Model
{
    private $table = 'provinsi_tbl';

    public function findAll($columns = '*')
    {
        $this->db->select($columns);

        $this->db->from($this->table);

        return $this->db->get()->result_array();
    }
}