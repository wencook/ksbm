<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_model extends CI_Model
{
    var $table = "USER_REGISTRATION";
    var $column_order = array(null, 'NAME', 'EMAIL', 'HP', "STATUS_REGISTER"); //set column field database for datatable orderable
    var $column_search = array('NAME', 'EMAIL', 'HP', "STATUS_REGISTER"); //set column field database for datatable searchable
    var $order = array('ID' => 'DESC'); // default order

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query($post)
    {
        $this->db->where("STATUS_VERIFIKASI_EMAIL", 1); // Tampilkan yang hanya sudah memverifikasi emailnya
        $this->db->select("ID, NAME, EMAIL, HP, CASE WHEN STATUS_REGISTER = 1 THEN 'Approve' WHEN STATUS_REGISTER = 0 THEN 'Not Approve' END AS STATUS_REGISTER");
        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if($post['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $post['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $post['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($post['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$post['order']['0']['column']], $post['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($post)
    {
        $this->_get_datatables_query($post);
        if($post['length'] != -1)
        {
            $this->db->limit($post['length'], $post['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($post)
    {
        $this->_get_datatables_query($post);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_user_registration($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get($this->table);
    }

    function update_data ($data, $where)
    {
        foreach ($where as $key => $value)
        {
            $this->db->where($key, $value);
        }
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    function approval_data($data, $where)
    {
        $this->db->trans_start();

        // Update table USER_REGISTRATION
        foreach ($where as $key => $value)
        {
            $this->db->where($key, $value);
        }
        $this->db->update($this->table, $data);
        // return $this->db->affected_rows();

        // simpan ke table ADMIN_USER_TBL
        $wu["ID"] = $data["ID"];
        $ruser = $this->get_user_registration($wu)->row();
        // echo "<pre>";var_dump($ruser);echo "</pre>";exit;
        $data_au["EMAIL"] = $ruser->EMAIL;
        $data_au["PASSWORD"] = random_string("alpha", "8");
        $data_au["PERAN"] = $ruser->PERAN;
        $this->db->insert('ADMIN_USER_TBL', $data_au);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            return "Error";
        }
        else
        {
            return 1;
        }
    }

    function get_detail_user_registration($where = NULL)
    {
        $this->db->select('ur.ID AS ID_UR, ur.NAME, ur.EMAIL, ur.HP, p.PERAN, pr.name AS name_provinsi, k.name AS name_kabupaten, kc.name AS name_kecamatan, kl.name AS name_kelurahan, au.PASSWORD');
        $this->db->join('USER_REGISTRATION_KEAHLIAN_TBL urk', 'ur.ID = urk.USER_REGISTRATION_ID');
        $this->db->join('PERAN_TBL p', 'ur.PERAN = p.ID');
        $this->db->join('provinsi_tbl pr', 'ur.PROVINSI = pr.id');
        $this->db->join('kabupaten_tbl k', 'ur.KABUPATEN = k.id');
        $this->db->join('kecamatan_tbl kc', 'ur.KECAMATAN = kc.id');
        $this->db->join('kelurahan_tbl kl', 'ur.KELURAHAN = kl.id');
        $this->db->join('ADMIN_USER_TBL au', 'ur.EMAIL = au.EMAIL');

        if ($where != NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($where, $value);
            }
        }

        return $this->db->get('USER_REGISTRATION ur');
    }
}
