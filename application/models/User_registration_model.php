<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_registration_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get_provinsi($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('provinsi_tbl');
    }

    function get_kabupaten($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('kabupaten_tbl');
    }

    function get_kecamatan($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('kecamatan_tbl');
    }

    function get_kelurahan($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('kelurahan_tbl');
    }

    function save($data)
    {
        // echo "<pre>";var_dump($data);echo "</pre>";exit;
        $this->db->trans_start();

        // insert ke table USER_REGISTRATION
        $data_ur["NAME"] = $data["txt_name"];
        $data_ur["EMAIL"] = $data["txt_email"];
        $data_ur["HP"] = $data["txt_hp"];
        $data_ur["PROVINSI"] = $data["opt_provinsi"];
        $data_ur["KABUPATEN"] = $data["opt_kabupaten"];
        $data_ur["KECAMATAN"] = $data["opt_kecamatan"];
        $data_ur["KELURAHAN"] = $data["opt_kelurahan"];
        $data_ur["PERAN"] = $data["opt_peran"];
        $this->db->insert('USER_REGISTRATION', $data_ur);
        $id_user_registration = $this->db->insert_id();

        // insert ke table USER_REGISTRATION_KEAHLIAN
        foreach ($data["chk_keahlian"] as $key => $value)
        {
            $data_urk["USER_REGISTRATION_ID"] = $id_user_registration;
            $data_urk["KEAHLIAN_ID"] = $value;
            $this->db->insert('USER_REGISTRATION_KEAHLIAN_TBL', $data_urk);
        }

        $this->db->trans_complete();
        return $id_user_registration;
    }

    function get_user_registration($where = NULL)
    {
        if ($where != NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($where, $value);
            }
        }
        return $this->db->get('USER_REGISTRATION');
    }

    function get_peran($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('PERAN_TBL');
    }

    function get_keahlian($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('KEAHLIAN_TBL');
    }

    function update($data, $where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        $this->db->update("USER_REGISTRATION", $data);
    }

    function get_detail_user_registration($where = NULL)
    {
        $this->db->select('ur.ID AS ID_UR, ur.NAME, ur.EMAIL, ur.HP, p.PERAN, pr.name AS name_provinsi, k.name AS name_kabupaten, kc.name AS name_kecamatan, kl.name AS name_kelurahan, au.PASSWORD');
        $this->db->join('USER_REGISTRATION_KEAHLIAN_TBL urk', 'ur.ID = urk.USER_REGISTRATION_ID');
        $this->db->join('PERAN_TBL p', 'ur.PERAN = p.ID');
        $this->db->join('provinsi_tbl pr', 'ur.PROVINSI = pr.id');
        $this->db->join('kabupaten_tbl k', 'ur.KABUPATEN = k.id');
        $this->db->join('kecamatan_tbl kc', 'ur.KECAMATAN = kc.id');
        $this->db->join('kelurahan_tbl kl', 'ur.KELURAHAN = kl.id');
        $this->db->join('ADMIN_USER_TBL au', 'ur.EMAIL = au.EMAIL');

        if ($where != NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($where, $value);
            }
        }

        return $this->db->get('USER_REGISTRATION ur');
    }
}
