<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_donasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function save($data)
    {
        $this->db->insert('RECEIVED_TBL', $data);
        return $this->db->affected_rows();
    }
}
