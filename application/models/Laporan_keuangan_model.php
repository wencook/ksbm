<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_keuangan_model extends CI_Model
{
    var $table = "EXPENSE_LOG_TBL";
    var $column_order = array(null, 'ACCOUNT_SENDER', 'NAME_SENDER', "AMMOUNT"); //set column field database for datatable orderable
    var $column_search = array('ACCOUNT_SENDER', 'NAME_SENDER', "AMMOUNT"); //set column field database for datatable searchable
    var $order = array('ID' => 'DESC'); // default order

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query($post)
    {
        $this->db->where('STATUS', 0);
        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if($post['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $post['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $post['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($post['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$post['order']['0']['column']], $post['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($post)
    {
        $this->_get_datatables_query($post);
        if($post['length'] != -1)
        {
            $this->db->limit($post['length'], $post['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($post)
    {
        $this->_get_datatables_query($post);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_mosque($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get($this->table);
    }

    function insert_data($data)
    {
        // echo $this->db->set($data)->get_compiled_insert($this->table);exit;
        $this->db->insert($this->table, $data);
        return $this->db->affected_rows();
    }

    function update_data ($data, $where)
    {
        foreach ($where as $key => $value)
        {
            $this->db->where($key, $value);
        }
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    function delete_data($where)
    {
        foreach ($where as $key => $value)
        {
            $this->db->where($key, $value);
        }
        $this->db->delete($this->table);
    }

    function get_kegiatan($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        $this->db->join('USER_REGISTRATION ur', 'cc.INSTRUCTOR = ur.EMAIL', 'left');
        $this->db->select('cc.ID, ur.NAME, cc.INSTRUCTOR, cc.TITLE');
        return $this->db->get('COURSE_CATALOGUE_TBL cc');
    }
}
