<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_kegiatan($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        $this->db->select("COURSE_PLAN_TBL.DATE, MOSQUE_TBL.NAMA, COURSE_CATALOGUE_TBL.TITLE");
        $this->db->join("COURSE_CATALOGUE_TBL", "COURSE_PLAN_TBL.COURSE_CATALOGUE_ID = COURSE_CATALOGUE_TBL.ID");
        $this->db->join("MOSQUE_TBL", "COURSE_PLAN_TBL.MOSQUE_ID=MOSQUE_TBL.ID");
        return $this->db->get('COURSE_PLAN_TBL');
    }

    function get_provinsi($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('provinsi_tbl');
    }

    function get_kabupaten($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('kabupaten_tbl');
    }

    function get_kecamatan($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('kecamatan_tbl');
    }

    function get_kelurahan($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        return $this->db->get('kelurahan_tbl');
    }

    function get_keahlian($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        $this->db->order_by("ID");
        return $this->db->get("KEAHLIAN_TBL");
    }
}
