<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peluang_infak_model extends CI_Model
{
    var $table = "COURSE_PLAN_TBL";
    var $column_order = array(null, 'KEAHLIAN', 'NAMA', "KEBUTUHAN"); //set column field database for datatable orderable
    var $column_search = array('KEAHLIAN', 'NAMA', "KEBUTUHAN"); //set column field database for datatable searchable
    var $order = array('cp.ID' => 'DESC'); // default order

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query($post)
    {
        $this->db->select('cp.ID, k.KEAHLIAN, m.NAMA, m.ALAMAT, (cp.BIAYA_TRAINER+cp.BIAYA_KONSUMSI+cp.BIAYA_ALAT) AS KEBUTUHAN');
        $this->db->join('MOSQUE_TBL m', 'cp.MOSQUE_ID = m.ID');
        $this->db->join('KEAHLIAN_TBL k', 'cp.BIDANG_ID = k.ID', "left");
        $this->db->join('USER_REGISTRATION ur', 'cp.USER_REGISTRATION_ID = ur.id');

        $this->db->from($this->table." cp");

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if($post['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $post['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $post['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($post['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$post['order']['0']['column']], $post['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($post)
    {
        $this->_get_datatables_query($post);
        if($post['length'] != -1)
        {
            $this->db->limit($post['length'], $post['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($post)
    {
        $this->_get_datatables_query($post);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_course_plan($where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        $this->db->select('cp.ID, k.KEAHLIAN, m.ID AS ID_MOSQUE, m.NAMA,m.ALAMAT,cp.BIAYA_TRAINER,cp.BIAYA_KONSUMSI,cp.BIAYA_ALAT,(cp.BIAYA_TRAINER+cp.BIAYA_KONSUMSI+cp.BIAYA_ALAT) AS KEBUTUHAN');
        $this->db->join('MOSQUE_TBL m', 'cp.MOSQUE_ID = m.ID');
        $this->db->join('KEAHLIAN_TBL k', 'cp.BIDANG_ID = k.ID');
        $this->db->join('USER_REGISTRATION ur', 'cp.USER_REGISTRATION_ID = ur.id');

        return $this->db->get('COURSE_PLAN_TBL cp');
    }
}
