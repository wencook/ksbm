<?php

class Titikpantau_repository
{
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->model('maps/titikpantau_model');
    }

    public function create(array $data)
    {
        $userId = $this->ci->titikpantau_model->create($data);

        return $userId;
    }

}