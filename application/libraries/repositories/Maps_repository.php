<?php

class Maps_repository
{
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->model('maps/provinsi_model');
        $this->ci->load->model('maps/kabupaten_model');
        $this->ci->load->model('maps/kecamatan_model');
        $this->ci->load->model('maps/kelurahan_model');
    }

    public function provinsi()
    {
        $columns = array(
            'id', 'name', 'latitude', 'longitude'
        );

        $data = $this->ci->provinsi_model->findAll(implode(', ', $columns));

        return $data;
    }

    public function kabupaten($id)
    {
        $columns = array(
            'id', 'name', 'latitude', 'longitude'
        );

        $data = $this->ci->kabupaten_model->findMuch($id, implode(', ', $columns));

        return $data;
    }

    public function kecamatan($id)
    {
        $columns = array(
            'id', 'name', 'latitude', 'longitude'
        );

        $data = $this->ci->kecamatan_model->findMuch($id, implode(', ', $columns));

        return $data;
    }

    public function kelurahan($id)
    {
        $columns = array(
            'id', 'name', 'latitude', 'longitude'
        );

        $data = $this->ci->kelurahan_model->findMuch($id, implode(', ', $columns));

        return $data;
    }

}