<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_registration extends MY_Controller
{
    var $modul = "user_registration";
    var $key = 'super-secret-key';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('user_registration_model','urm');
        $this->output->enable_profiler(FALSE);
    }

    function index()
    {
        $ldata = array();
        $this->load->helper("captcha");
        $this->load->helper("string");
        $captchaVals = array(
            'word' => random_string("numeric", 4),
            'img_path' => "./captcha/",
            'img_url' => base_url("captcha/"),
            'img_width' => 120,
            'img_height' => 40,
            'expiration' => 7200,
            'colors'        => array(
                'background' => array(210, 210, 210),
                'border' => array(150, 150, 150),
                'text' => array(100, 100, 100),
                'grid' => array(255, 140, 140)
            )
        );

        $myCap = create_captcha($captchaVals);

        $this->session->set_userdata('my_captcha', $myCap['word']);
        $data['cap'] = $myCap;

        $data["modul"] = $this->modul;
        $data["res_provinsi"] = $this->urm->get_provinsi()->result();
        $data["res_peran"] = $this->urm->get_peran(array("ID !=" => 0))->result();
        $data["res_keahlian"] = $this->urm->get_keahlian()->result();
        $data["content"] = "user_registration/user_registration";
		// $this->view('user_registration', $data);

        $ldata['content_block'] = $this->load->view('user_registration/user_registration', $data, true);
        $this->load->view('template/template_new', $ldata);
    }

    function ajax_kabupaten()
    {
        $id_provinsi = $this->input->post('id_provinsi');
        $where["provinsiId"] = $id_provinsi;
        $res_kabupaten = $this->urm->get_kabupaten($where)->result();
        $data["res_kabupaten"] = $res_kabupaten;
        echo "<option value=\"\">Pilih Kabupaten</option>";
        $this->load->view('user_registration/v_kabupaten', $data);
        $html = $this->output->get_output();
    }

    function ajax_kecamatan()
    {
        $id_kabupaten = $this->input->post('id_kabupaten');
        $where["kabupatenId"] = $id_kabupaten;
        $res_kecamatan = $this->urm->get_kecamatan($where)->result();
        $data["res_kecamatan"] = $res_kecamatan;
        echo "<option value=\"\">Pilih Kecamatan</option>";
        $this->load->view('user_registration/v_kecamatan', $data);
        $html = $this->output->get_output();
    }

    function ajax_kelurahan()
    {
        $id_kecamatan = $this->input->post('id_kecamatan');
        $where["kecamatanId"] = $id_kecamatan;
        $res_kelurahan = $this->urm->get_kelurahan($where)->result();
        $data["res_kelurahan"] = $res_kelurahan;
        echo "<option value=\"\">Pilih Kelurahan/Desa</option>";
        $this->load->view('user_registration/v_kelurahan', $data);
        $html = $this->output->get_output();
    }

    function save_registration()
    {
        $user_captcha = set_value("txt_captcha");
        $check_captcha = $this->validate_captcha($user_captcha);

        if ($check_captcha === TRUE)
        {
            // echo "<pre>";var_dump($this->input->post());echo "</pre>";exit;

            // check data di database
            $wd["NAME"] = $this->input->post('txt_name');
            $wd["EMAIL"] = $this->input->post('txt_email');
            $wd["HP"] = $this->input->post('txt_hp');
            $wd["PROVINSI"] = $this->input->post('opt_provinsi');
            $wd["KABUPATEN"] = $this->input->post('opt_kabupaten');
            $wd["KECAMATAN"] = $this->input->post('opt_kecamatan');
            $wd["KELURAHAN"] = $this->input->post('opt_kelurahan');
            $wd["PERAN"] = $this->input->post('opt_peran');
            $check = $this->urm->get_user_registration($wd)->num_rows();

            if ($check > 0)
            {
                echo "Data Sudah Ada";
                redirect("user_registration", "refresh");
            }
            else
            {
                $save = $this->urm->save($this->input->post());

                $w["ur.ID"] = $save;
                $row = $this->urm->get_detail_user_registration($w)->row();

                $data["row"] = $row;

                // send email
                $this->load->library("email");

                $config_email["protocol"] = "smtp";
                $config_email["smtp_host"] = "ssl://smtp.gmail.com";
                $config_email["smtp_port"] = "465";
                $config_email["smtp_user"] = "ksbmtest@gmail.com";
                $config_email['smtp_pass'] = "KsbmPass!";
                $config_email['charset']   = 'utf-8';
                $config_email['mailtype'] = 'html';
                $config_email["priority"] = 1;
                $config_email["newline"] = "\r\n";
                $this->email->initialize($config_email);

                $this->email->from('ksbm@gmail.com', 'Administrator KSBM');
                $this->email->to($row->EMAIL);
                $this->email->subject("Verifikasi Registration");
                $this->email->message($this->load->view("user_registration/notif_email", $data, TRUE));
                $this->email->send();
                $this->email->print_debugger();

                redirect("user_registration/complete_registration");
            }
        }
        else
        {
            redirect("user_registration", "refresh");
        }
    }

    public function validate_captcha($captcha)
    {
        if (strcmp($captcha, $this->session->userdata("my_captcha")) === 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function complete_registration()
    {
        $data["content"] = "complete_registration";
        $this->load->view("user_registration/complete_registration", $data);
    }

    function aktivasi($id)
    {
        if ($id)
        {
            $w["ID"] = $id;
            $data["STATUS_VERIFIKASI_EMAIL"] = 1;
            $this->urm->update($data, $w);
            $this->load->view($this->modul."/verifikasi_email");
        }
        else
        {
            redirect($this->modul);
        }
    }
}
