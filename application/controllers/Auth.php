<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller
{
    /*
     * @author zulyantara <zulyantara@gmail.com>
     * @copyright copyright 2016 zulyantara
     */

	function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(FALSE);
		$this->load->model("user_model", "mm");
	}

    function index()
    {
        if($this->session->userdata('isLoggedIn') !== TRUE)
        {
            $this->load->view('login/home');
        }
        else
        {
        	redirect("admin_home");
        }
    }

    function validate_credential()
    {
		if($this->input->post('btn_login') === 'btn_login')
		{
    			$query = $this->mm->validate($this->input->post('txt_username'), $this->input->post('txt_password'));
                // echo "<pre>";var_dump($query);echo "</pre>";exit;
    			if($query != FALSE)
    			{
    				$data = array(
    					'userId' => $query->ID,
                        'userEmail' => $query->EMAIL,
						"userPeran" => $query->PERAN,
    					'isLoggedIn' => TRUE
    				);
    				$this->session->set_userdata($data);
    				redirect("admin_home");
    			}
    			else
    			{
    				$data['alert'] = "Username or Password Wrong!";
    				$this->load->view('login/home', $data);
    			}
		}
		else
		{
            $data["alert"] = "Username or Password Wrong!";
            $this->load->view('login/home', $data);
		}
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }

    function profil()
    {
        $id = $this->session->userdata('userId');
        $data["row_user"] = $this->mm->get_user_by_id($id);
        $this->view('login/profil', $data);
    }

    function save_profil()
    {

        $id = $this->input->post('user_id');
        //$nama = $this->input->post('user_nama');
        $email = $this->input->post('user_email');
        $password= $this->input->post('user_password');
        $btn = $this->input->post('btn_save');

        if ($btn === "btn_ubah")
        {
            $this->session->set_flashdata("alert", "success");
            $this->session->set_flashdata("keterangan", $nama);
            $this->session->set_flashdata("action", "simpan");

            $where["user_id"] = $id;
            //$data["user_nama"] = $nama;
            $data["user_email"] = $email;
            if ($password !== "")
            {
                $data["user_password"] = password_hash($password, PASSWORD_DEFAULT);
            }
            $this->mm->update_data($data, $where);
            redirect('auth/profil','refresh');
        }
        else
        {
            redirect('home','refresh');
        }
    }
}
