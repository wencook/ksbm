<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_pelatihan extends MY_Controller
{
    var $modul = "jadwal_pelatihan";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("jadwal_pelatihan_model", "tm");
    }

    function index()
    {
        $data["pr"] = "<a href=\"".site_url($this->modul.'/tambah_data')."\" class=\"btn btn-warning btn-sm\">Tambah Data</a>
        <button type=\"button\" title=\"Refresh\" onclick=\"reload_table();\" class=\"btn btn-warning btn-sm\">Refresh</button>";
        $data["modul"] = $this->modul;
        $data["ph"] = "Jadwal Pelatihan";
        $this->view("home", $data);
    }

    public function ajax_list()
    {
        $list = $this->tm->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $dsbl = $r->STATUS == 'Approve' ? "disabled=\"disabled\"" : "";
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = date("d-m-Y", strtotime($r->DATE));
            $row[] = $r->NAMA_MOSQUE;
            $row[] = $r->NAMA_COURSE;
            $row[] = $r->STATUS;
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/approve/".$r->ID)."\" class=\"btn btn-primary btn-xs btn-flat\" $dsbl>Approve</a>&nbsp;<a href=\"".site_url($this->modul."/edit_data/".$r->ID)."\" class=\"btn btn-warning btn-flat btn-xs\">Edit</a>&nbsp;<a href=\"".site_url($this->modul."/delete_data/".$r->ID)."\" class=\"btn btn-danger btn-xs btn-flat\">Delete</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->tm->count_all();
        $output["recordsFiltered"] = $this->tm->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function tambah_data()
    {
        $data["MOSQUE_ID"] = $this->input->post('opt_mosque');
        $data["BIDANG_ID"] = $this->input->post('opt_bidang');
        $data["USER_REGISTRATION_ID"] = $this->input->post('opt_user');
        $data["COURSE_CATALOGUE_ID"] = $this->input->post('opt_course');
        $data["DATE"] = $this->input->post('txt_date');
        $data["BIAYA_TRAINER"] = $this->input->post('txt_biaya_trainer');
        $data["BIAYA_KONSUMSI"] = $this->input->post('txt_biaya_konsumsi');
        $data["BIAYA_ALAT"] = $this->input->post('txt_biaya_alat');

        if ($this->input->post('btn_save') === "save")
        {
            $simpan = $this->tm->insert_data($data);
            if ($simpan > 0)
            {
                redirect($this->modul, "refresh");
            }
            else
            {
                redirect($this->modul."/tambah_data", "refresh");
            }
        }
        elseif ($this->input->post('btn_save') === "update")
        {
            $where["ID"] = $this->input->post('txt_id');
            $simpan = $this->tm->update_data($data, $where);
            if ($simpan > 0)
            {
                redirect($this->modul, "refresh");
            }
            else
            {
                redirect($this->modul."/tambah_data", "refresh");
            }
        }
        else
        {
            $data["res_mosque"] = $this->tm->get_mosque()->result();
            $data["res_keahlian"] = $this->tm->get_keahlian()->result();
            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">List Masjid</a>";
            $data["modul"] = $this->modul;
            $data["url"] = "tambah_data";
            $data["ph"] = "Jadwal Pelatihan";
            $data["pd"] = "Form Pelatihan";
            $this->view("form", $data);
        }
    }

    function edit_data($id = NULL)
    {
        if ($id !== NULL)
        {
            $where["ID"] = $id;
            $result = $this->tm->get_training($where)->row();
            $data["res_mosque"] = $this->tm->get_mosque()->result();
            $data["res_keahlian"] = $this->tm->get_keahlian()->result();
            $data["r_training"] = $result;
            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">List Masjid</a>";
            $data["modul"] = $this->modul;
            $data["url"] = "tambah_data";
            $data["ph"] = "Jadwal Pelatihan";
            $data["pd"] = "Form Pelatihan";
            $this->view("form", $data);
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }

    function approve($id)
    {
        if ($id)
        {
            $where["ID"] = $id;
            $data["STATUS"] = 1;
            $this->tm->update_data($data, $where);
            redirect($this->modul, "refresh");
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }

    function ajax_trainer()
    {
        $id_bidang = $this->input->post('id_bidang');
        $where["urk.KEAHLIAN_ID"] = $id_bidang;
        $rt = $this->tm->get_user($where)->result();

        echo "<option value=\"\">Pilih Trainer</option>";
        if ($rt)
        {
            foreach ($rt as $key => $value)
            {
                echo "<option value=\"".$value->ID."\">".$value->NAME."</option>";
            }
        }
    }

    function ajax_course()
    {
        $id_bidang = $this->input->post('id_bidang');
        $id_user = $this->input->post('id_user');
        $where["cc.BIDANG"] = $id_bidang;
        $where["ur.ID"] = $id_user;
        $rt = $this->tm->get_course($where)->result();

        echo "<option value=\"\">Pilih Trainer</option>";
        if ($rt)
        {
            foreach ($rt as $key => $value)
            {
                echo "<option value=\"".$value->ID."\">".$value->TITLE."</option>";
            }
        }
    }
}
