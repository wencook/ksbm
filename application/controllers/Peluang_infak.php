<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peluang_infak extends MY_Controller
{
    var $modul = "peluang_infak";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('peluang_infak_model','pim');
        $this->output->enable_profiler(FALSE);
    }

    function index()
    {
        // $res_course = $this->pim->get_course_plan()->result();
        // $data["res_course"] = $res_course;
        $data["modul"] = $this->modul;
        $this->load->view("peluang_infak/peluang_infak", $data);
    }

    public function ajax_list()
    {
        $list = $this->pim->get_datatables($this->input->post());
        $data = array();
        foreach ($list as $r)
        {
            $row = array();
            $row[] = $r->KEAHLIAN;
            $row[] = $r->NAMA;
            $row[] = $r->ALAMAT;
            $row[] = $r->KEBUTUHAN;
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/view_data/".$r->ID)."\" class=\"btn btn-warning btn-flat btn-xs\">View</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->pim->count_all();
        $output["recordsFiltered"] = $this->pim->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function view_data($id)
    {
        if ($id)
        {
            $where["cp.ID"] = $id;
            $row = $this->pim->get_course_plan($where)->row();

            $array = array(
                'idMosque' => $row->ID_MOSQUE
            );
            $this->session->set_userdata($array);

            $data["row"] = $row;
            $this->load->view('peluang_infak/detail', $data);
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }
}
