<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_kegiatan extends MY_Controller
{
    var $modul = "laporan_kegiatan";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("laporan_kegiatan_model", "tm");
    }

    function index()
    {
        $data["pr"] = "<button type=\"button\" title=\"Refresh\" onclick=\"reload_table();\" class=\"btn btn-warning btn-sm\">Refresh</button>";
        $data["modul"] = $this->modul;
        $data["ph"] = "Laporan Kegiatan";
        $this->view("home", $data);
    }

    public function ajax_list()
    {
        $list = $this->tm->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = date("d-m-Y", strtotime($r->DATE));
            $row[] = $r->NAMA_MOSQUE;
            $row[] = $r->NAMA_COURSE;
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/laporan/".$r->ID)."\" class=\"btn btn-primary btn-xs btn-flat\">Laporan Kegiatan</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->tm->count_all();
        $output["recordsFiltered"] = $this->tm->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function tambah_data($id, $val, $id2)
    {
        if ($id)
        {
            $data["COURSE_PLAN_ID"] = $id;
            $data["COURSE_TRAINEE_ID"] = $id2;
            $data["PRESENCE"] = $val;
            $simpan = $this->tm->insert_data($data);

            if ($simpan > 0)
            {
                redirect($this->modul."/laporan/".$id, "refresh");
            }
            else
            {
                redirect($this->modul."/laporan/".$id, "refresh");
            }
        }
    }

    function laporan($id)
    {
        if ($id)
        {
            $w["cp.ID"] = $id;
            $row = $this->tm->get_course($w)->row();
            $data["row"] = $row;

            // get peserta
            $wp["COURSE_PLAN_ID"] = $row->ID;
            $res_peserta = $this->tm->get_course_trainee($wp)->result();
            $data["res_peserta"] = $res_peserta;

            // get expense_log
            $we["COURSE_PLAN_ID"] = $row->ID;
            $res_expense = $this->tm->get_expense_log($we)->result();
            $data["res_expense"] = $res_expense;

            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">Kembali</a>";
            $data["modul"] = $this->modul;
            $data["url"] = $this->modul."/tambah_data/".$row->ID;
            $data["ph"] = "Laporan Kegiatan";
            $data["pd"] = "Presensi Peserta";
            $this->view("laporan", $data);
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }
}
