<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_keuangan extends MY_Controller
{
    var $modul = "laporan_keuangan";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("laporan_keuangan_model", "mrm");
    }

    function index()
    {
        $data["res_kegiatan"] = $this->mrm->get_kegiatan()->result();
        $data["url"] = "save_data";
        $data["modul"] = $this->modul;
        $data["ph"] = "Laporan Keuangan";
        $this->view("form", $data);
    }

    public function ajax_list()
    {
        $list = $this->mrm->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = $r->ACCOUNT_SENDER;
            $row[] = $r->NAME_SENDER;
            $row[] = "<div class=\"text-right\">".number_format($r->AMOUNT,0, ",",".")."</div>";
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/edit_data/".$r->ID)."\" class=\"btn btn-warning btn-flat btn-xs\">Received</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->mrm->count_all();
        $output["recordsFiltered"] = $this->mrm->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function save_data()
    {
        if ($this->input->post('btn_save') === "save")
        {
            $data["COURSE_PLAN_ID"] = $this->input->post('opt_kegiatan');
            $data["BIAYA_KELUAR"] = $this->input->post('txt_biaya_keluar');
            $data["TANGGAL"] = $this->input->post('txt_tanggal');
            $data["KEPERLUAN"] = $this->input->post('opt_keperluan');
            $data["CATATAN"] = $this->input->post('txt_catatan');
            $this->mrm->insert_data($data);
            redirect($this->modul, "refresh");
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }

    function edit_data($id = NULL)
    {
        if ($id !== NULL)
        {
            $where["ID"] = $id;
            $data["STATUS"] = 1;
            $result = $this->mrm->update_data($data,$where);

            redirect($this->modul, "refresh");
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }

    function ajax_trainer()
    {
        $where["cc.ID"] = $this->input->post('id_kegiatan');
        $r = $this->mrm->get_kegiatan($where)->row();
        echo $r->NAME;
    }
}
