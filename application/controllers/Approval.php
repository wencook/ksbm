<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends MY_Controller
{
    var $modul = "approval";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("approval_model", "am");
        $this->load->helper("string");
    }

    function index()
    {
        $data["ph"] = "Approval";
        $data["modul"] = $this->modul;
        $this->view("home", $data);
    }

    public function ajax_list()
    {
        $list = $this->am->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $disabled_status = $r->STATUS_REGISTER == "Approve" ? "disabled" : "";
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = $r->NAME;
            $row[] = $r->EMAIL;
            $row[] = $r->HP;
            $row[] = "<div class=\"text-center\"><span class=\"label label-primary\">".$r->STATUS_REGISTER."</span></div>";
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/approve/".$r->ID)."\" class=\"btn btn-primary btn-flat btn-xs $disabled_status\">Approve</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->am->count_all();
        $output["recordsFiltered"] = $this->am->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function approve($id)
    {
        if ($id)
        {
            $where["ID"] = $id;
            $data["ID"] = $id;
            $data["STATUS_REGISTER"] = 1;
            $update = $this->am->approval_data($data, $where);
            if ($update > 0)
            {
                // get data
                $w["ur.ID"] = $id;
                $row = $this->am->get_detail_user_registration($w)->row();
                $data["row"] = $row;

                // send email
                $this->load->library("email");

                $config_email["protocol"] = "smtp";
                $config_email["smtp_host"] = "ssl://smtp.gmail.com";
                $config_email["smtp_port"] = "465";
                $config_email["smtp_user"] = "ksbmtest@gmail.com";
                $config_email['smtp_pass'] = "KsbmPass!";
                $config_email['charset']   = 'utf-8';
                $config_email['mailtype'] = 'html';
                $config_email["priority"] = 1;
                $config_email["newline"] = "\r\n";
                $this->email->initialize($config_email);

                $this->email->from('ksbm@gmail.com', 'Administrator KSBM');
                $this->email->to($row->EMAIL);
                $this->email->subject("Approval Registration");
                $this->email->message($this->load->view("approval/notif_email", $data, TRUE));
                $this->email->send();
                $this->email->print_debugger();

                redirect($this->modul."/index/1", "refresh");
            }
            else
            {
                redirect($this->modul."/index/0", "refresh");
            }
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }
}
