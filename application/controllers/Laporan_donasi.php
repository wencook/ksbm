<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_donasi extends MY_Controller
{
    var $modul = "laporan_donasi";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('laporan_donasi_model','urm');
        $this->output->enable_profiler(FALSE);
    }

    function index()
    {
        $data["modul"] = $this->modul;
        $data["ph"] = "Laporan Donasi";
        $this->view("home", $data);
    }

    function save_data()
    {
        if ($this->input->post('btn_save') === "save")
        {
            // echo "<pre>";var_dump($this->input->post());echo "</pre>";exit;

            // check data di database
            $wd["ACCOUNT_SENDER"] = $this->input->post('txt_no_account');
            $wd["NAME_SENDER"] = $this->input->post('txt_nama');
            $wd["AMOUNT"] = $this->input->post('txt_jumlah');
            $wd["STATUS"] = "0";

            $this->urm->save($wd);
            $this->complete_registration();
        }
        else
        {
            redirect("peluang_infak", "refresh");
        }
    }

    function complete_registration()
    {
        $data["content"] = "complete_registration";
        $this->load->view("user_registration/complete_registration", $data);
    }
}
