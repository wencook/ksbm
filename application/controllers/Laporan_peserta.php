<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_peserta extends MY_Controller
{
    var $modul = "laporan_peserta";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("laporan_peserta_model", "tm");
    }

    function index()
    {
        $data["pr"] = "<button type=\"button\" title=\"Refresh\" onclick=\"reload_table();\" class=\"btn btn-warning btn-sm\">Refresh</button>";
        $data["modul"] = $this->modul;
        $data["ph"] = "Laporan Peserta";
        $this->view("home", $data);
    }

    public function ajax_list()
    {
        $list = $this->tm->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = date("d-m-Y", strtotime($r->DATE));
            $row[] = $r->NAMA_MOSQUE;
            $row[] = $r->NAMA_COURSE;
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/laporan/".$r->ID)."\" class=\"btn btn-primary btn-xs btn-flat\">Laporan Instructor</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->tm->count_all();
        $output["recordsFiltered"] = $this->tm->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function tambah_data()
    {
        if ($this->input->post('btn_save') === "save")
        {
            $course_plan_id = $this->input->post('txt_id_course');
            $course_catalogue_id = $this->input->post('txt_id_catalogue');
            $penguasaan_materi = $this->input->post('opt_penguasaan_materi');
            $dipahami = $this->input->post('opt_dipahami');
            $praktikum = $this->input->post('opt_praktikum');
            foreach ($course_catalogue_id as $key => $value)
            {
                $data["COURSE_PLAN_ID"] = $course_plan_id;
                $data["COURSE_CATALOGUE_ID"] = $course_catalogue_id[$key];
                $data["PENGUASAAN_MATERI"] = $penguasaan_materi[$key];
                $data["DIPAHAMI"] = $dipahami[$key];
                $data["PRAKTIKUM"] = $praktikum[$key];
                $simpan = $this->tm->insert_data($data);
            }

            if ($simpan > 0)
            {
                redirect($this->modul."/laporan/".$course_plan_id, "refresh");
            }
            else
            {
                redirect($this->modul."/laporan/".$id, "refresh");
            }
        }
    }

    function laporan($id)
    {
        if ($id)
        {
            $w["cp.ID"] = $id;
            $row = $this->tm->get_course($w)->row();
            $data["row"] = $row;

            // get materi
            // $wp["ct.COURSE_PLAN_ID"] = $row->ID;
            // $res_materi = $this->tm->get_course_catalogue($wp)->result();
            // $data["res_materi"] = $res_materi;

            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">Kembali</a>";
            $data["modul"] = $this->modul;
            $data["url"] = $this->modul."/tambah_data/";
            $data["ph"] = "Laporan Kegiatan";
            $data["pd"] = "Presensi Peserta";
            $this->view("laporan", $data);
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }
}
