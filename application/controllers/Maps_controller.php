<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Maps_controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->config('app');
        $this->load->helper('app');
        $this->load->library('form_validation');
        $this->load->library('repositories/maps_repository', 'maps_repository');
        $this->load->library('repositories/titikpantau_repository', 'titikpantau_repository');
    }

    public function index()
    {
    	$data = $this->maps_repository->provinsi();

//         $this->load->view('front/elements/head');
//         $this->load->view('front/maps/index', compact('data', 'footScripts'));
    	$this->view('front/maps/index', compact('data', 'footScripts'));
    }

    public function store()
    {
        $data = array(
            'alamat' => $this->input->post('alamat'),
            'latitude' => $this->input->post('latitude'),
            'longitude' => $this->input->post('longitude'),
            'masalah' => $this->input->post('masalah'),
        );

        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('latitude', 'latitude', 'required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required');
        $this->form_validation->set_rules('masalah', 'Masalah', 'required');
        $this->form_validation->run();

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $this->session->set_flashdata('errors', $errors);

            foreach ($data as $key => $value) {
                $this->session->set_flashdata($key, $value);
            }

            redirect('maps');
        }

        $this->titikpantau_repository->create($data);

        $this->session->set_flashdata('success', sprintf('Berhasil menambah data masalah di alamat <b>%s</b>.', $data['alamat']));

        redirect('maps');
    }

    public function kabupaten()
	{
		$prov_id = $this->uri->segment(3);

		$kabupaten = $this->maps_repository->kabupaten($prov_id);

		echo json_encode($kabupaten);
	}

	public function kecamatan()
	{
		$prov_id = $this->uri->segment(3);

		$kecamatan = $this->maps_repository->kecamatan($prov_id);

		echo json_encode($kecamatan);
	}

	public function kelurahan()
	{
		$prov_id = $this->uri->segment(3);

		$kelurahan = $this->maps_repository->kelurahan($prov_id);

		echo json_encode($kelurahan);
	}

}
