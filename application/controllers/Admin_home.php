<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_home extends MY_Controller
{
    var $modul = "admin_home";
    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    }

    function index()
    {
        $this->view("home");
    }

}
