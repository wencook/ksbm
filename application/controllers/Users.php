<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{
    protected $modul = 'users';

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
        $this->load->model('user_model','mm');
        $this->output->enable_profiler(FALSE);
	}

    public function index()
    {
        $data["res_user"] = $this->mm->get_data();
		$this->view('home', $data);
    }

    public function form_new()
    {
		$this->view('form');
    }

    public function form_edit()
    {
        $id = $this->input->get("id");

        $data['res_users'] = $this->mm->get_user_by_id($id);
		$this->view('form', $data);
    }

    public function save()
    {
        $id = $this->input->post("user_id");
        $this->input->post("user_password") === "" ? "" : $arr_data["user_password"] = password_hash($this->input->post("user_password"), PASSWORD_DEFAULT);
        $arr_data["user_email"] = $this->input->post("user_email");
        $button = $this->input->post("btn_save");

        // echo "<pre>";var_dump($arr_data);echo "</pre>";exit;

        $this->session->set_flashdata("alert", "success");
        $this->session->set_flashdata("keterangan", $arr_data["user_username"]);
        $this->session->set_flashdata("action", "simpan");

        // Jika data baru maka ...
        if  ($button === "btn_simpan")
        {
            // Pertama cek data apakah ada duplikat?
            $arr_like["user_email"] = $arr_data["user_email"];
            $check_data = $this->mm->check_duplicate($arr_like);

            // Jika tidak ada double data maka simpan data
            if ($check_data === 0)
            {
                $this->mm->insert_data($arr_data);
                redirect($this->modul);
            }
            else
            {
                ?>
                <script>
                alert("Oops ... Data <?php echo strtoupper($arr_data["user_email"]); ?> sudah ada");
                window.history.back();
                </script>
                <?php
            }
        }
        // Jika edit data maka ...
        elseif ($button === "btn_ubah")
        {
            $this->mm->update_data($arr_data, array('user_id' => $id));
            redirect($this->modul);
        }
        else
        {
            redirect($this->modul);
        }
    }

    public function delete_data()
	{
		$id = $this->input->get("id");

        $this->session->set_flashdata("alert", "warning");
        $this->session->set_flashdata("keterangan", $arr_id[2]);
        $this->session->set_flashdata("action", "hapus");

        // hapus data di table menu
		$this->mm->delete_data(array('user_id' => $id));

		redirect($this->modul);
	}

}
