<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends MY_Controller
{
    var $modul = "course";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("course_model", "tm");
    }

    function index()
    {
        $data["pr"] = "<button type=\"button\" title=\"Refresh\" onclick=\"reload_table();\" class=\"btn btn-warning btn-sm\">Refresh</button>";
        $data["modul"] = $this->modul;
        $data["ph"] = "Course";
        $this->view("home", $data);
    }

    public function ajax_list()
    {
        $list = $this->tm->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = date("d-m-Y", strtotime($r->DATE));
            $row[] = $r->NAMA_MOSQUE;
            $row[] = $r->NAMA_COURSE;
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/peserta/".$r->ID)."\" class=\"btn btn-primary btn-xs btn-flat\">Peserta</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->tm->count_all();
        $output["recordsFiltered"] = $this->tm->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function tambah_data($id = NULL)
    {
        if ($id !== NULL)
        {
            $data["id_course"] = $id;
            $data["pr"] = "<a href=\"".site_url($this->modul."/peserta/".$id)."\" class=\"btn btn-warning btn-sm\">List Kursus</a>";
            $data["modul"] = $this->modul;
            $data["url"] = $this->modul."/tambah_data";
            $data["ph"] = "Jadwal Pelatihan";
            $data["pd"] = "Form Pelatihan";
            $this->view("form", $data);
        }
        else
        {
            $data["COURSE_PLAN_ID"] = $this->input->post('txt_id_course');
            $data["NAMA"] = $this->input->post('txt_nama');
            $data["EMAIL"] = $this->input->post('txt_email');
            $data["HP"] = $this->input->post('txt_hp');
            $data["STATUS"] = 0;

            if ($this->input->post('btn_save') === "save")
            {
                $simpan = $this->tm->insert_data($data);
                if ($simpan > 0)
                {
                    $wc["cp.ID"] = $this->input->post('txt_id_course');
                    $wc["NAMA_PESERTA"] = $this->input->post("txt_nama");
                    $wc["ct.HP"] = $this->input->post("txt_hp");
                    $wc["EMAIL_PESERTA"] = $this->input->post("txt_email");
                    $row = $this->tm->get_course($wc)->row();
                    $data["row"] = $row;

                    // kirim email
                    $this->load->library("email");

                    $config_email["protocol"] = "smtp";
                    $config_email["smtp_host"] = "ssl://smtp.gmail.com";
                    $config_email["smtp_port"] = "465";
                    $config_email["smtp_user"] = "ksbmtest@gmail.com";
                    $config_email['smtp_pass'] = "KsbmPass!";
                    $config_email['charset']   = 'utf-8';
                    $config_email['mailtype'] = 'html';
                    $config_email["priority"] = 1;
                    $config_email["newline"] = "\r\n";
                    $this->email->initialize($config_email);

                    $this->email->from('ksbm@gmail.com', 'Administrator KSBM');
                    $this->email->to($data["EMAIL"]);
                    $this->email->subject("Undangan Pelatihan ".$row->TITLE);
                    $this->email->message($this->load->view($this->modul."/notif_email", $data, TRUE));
                    $this->email->send();
                    $this->email->print_debugger();

                    redirect($this->modul."/peserta/".$this->input->post('txt_id_course'), "refresh");
                }
                else
                {
                    redirect($this->modul, "refresh");
                }
            }
            elseif ($this->input->post('btn_save') === "update")
            {
                $where["ID"] = $this->input->post('txt_id');
                $simpan = $this->tm->update_data($data, $where);
                if ($simpan > 0)
                {
                    redirect($this->modul, "refresh");
                }
                else
                {
                    redirect($this->modul, "refresh");
                }
            }
            else
            {
                redirect($this->modul, "refresh");
            }
        }
    }

    function peserta($id)
    {
        if ($id)
        {
            $w["cp.ID"] = $id;
            $row = $this->tm->get_course($w)->row();

            // get peserta
            $wp["COURSE_PLAN_ID"] = $row->ID;
            $res_peserta = $this->tm->get_course_trainee($wp)->result();
            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">Kembali</a>";

            $data["res_peserta"] = $res_peserta;
            $data["row"] = $row;
            $data["modul"] = $this->modul;
            $data["url"] = $this->modul."/tambah_data/".$row->ID;
            $data["ph"] = "Course";
            $data["pd"] = "Daftar Peserta";
            $this->view("peserta", $data);
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }

    function aktivasi_peserta($id)
    {
        if ($id)
        {
            $where = "ID=$id";
            $data["STATUS"] = 1;
            $update = $this->tm->update_peserta($where, $data);
            if ($update > 0)
            {
                // get data peserta
                $wrt["ID"] = $id;
                $row_trainee = $this->tm->get_course_trainee($wrt)->row();

                // cek table admin_user_tbl, udah ada kah data peserta?
                $wau["EMAIL"] = $row_trainee->EMAIL;
                $row_user = $this->tm->get_admin_user($wau)->row();
                var_dump($row_user);exit;

                if ($row_user)
                {

                }

                $this->load->view($this->modul."/complete_registration");
            }
        }
        else
        {
            redirect($this->modul);
        }
    }
}
