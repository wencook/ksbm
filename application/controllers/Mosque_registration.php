<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mosque_registration extends MY_Controller
{
    var $modul = "mosque_registration";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("mosque_registration_model", "mrm");

        $this->load->config('app');
        $this->load->helper('app');
        $this->load->library('form_validation');
        $this->load->library('repositories/maps_repository', 'maps_repository');
        $this->load->library('repositories/titikpantau_repository', 'titikpantau_repository');
    }

    function index()
    {
        $data["pr"] = "<a href=\"".site_url($this->modul.'/tambah_data')."\" class=\"btn btn-warning btn-sm\">Tambah Data</a>
        <button type=\"button\" title=\"Refresh\" onclick=\"reload_table();\" class=\"btn btn-warning btn-sm\">Refresh</button>";
        $data["modul"] = $this->modul;
        $data["ph"] = "Masjid";
        $this->view("home", $data);
    }

    public function ajax_list()
    {
        $list = $this->mrm->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = $r->NAMA;
            $row[] = $r->ALAMAT;
            $row[] = $r->DKM_KETUA;
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/edit_data/".$r->ID)."\" class=\"btn btn-warning btn-flat btn-xs\">Edit</a>&nbsp;<a href=\"".site_url($this->modul."/delete_data/".$r->ID)."\" class=\"btn btn-danger btn-xs btn-flat\">Delete</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->mrm->count_all();
        $output["recordsFiltered"] = $this->mrm->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function tambah_data()
    {
        $data["NAMA"] = $this->input->post('txt_nama');
        $data["PROVINSI"] = $this->input->post('provinsi');
        $data["KABUPATEN"] = $this->input->post('kabupaten');
        $data["KECAMATAN"] = $this->input->post('kecamatan');
        $data["KELURAHAN"] = $this->input->post('kelurahan');
        $data["LATITUDE"] = $this->input->post('latitude');
        $data["LONGITUDE"] = $this->input->post('longitude');
        $data["ALAMAT"] = $this->input->post('alamat');
        $data["DKM_KETUA"] = $this->input->post('txt_dkm_ketua');
        $data["REGISTERED_BY"] = $this->session->userdata("userEmail");

        if ($this->input->post('btn_save') === "save")
        {
            $simpan = $this->mrm->insert_data($data);
            if ($simpan > 0)
            {
                redirect($this->modul, "refresh");
            }
            else
            {
                redirect($this->modul."/tambah_data", "refresh");
            }
        }
        elseif ($this->input->post('btn_save') === "update")
        {
            $where["ID"] = $this->input->post('txt_id');
            $simpan = $this->mrm->update_data($data, $where);
            if ($simpan > 0)
            {
                redirect($this->modul, "refresh");
            }
            else
            {
                redirect($this->modul."/tambah_data", "refresh");
            }
        }
        else
        {
            $data["res_provinsi"] = $this->maps_repository->provinsi();
            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">List Masjid</a>";
            $data["modul"] = $this->modul;
            $data["url"] = "tambah_data";
            $data["ph"] = "Masjid";
            $data["pd"] = "Form Masjid";
            $this->view("form", $data);
        }
    }

    function edit_data($id = NULL)
    {
        if ($id !== NULL)
        {
            $where["ID"] = $id;
            $result = $this->mrm->get_mosque($where);
            $data["r_mosque"] = $result->row();
            $data["res_provinsi"] = $this->maps_repository->provinsi();
            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">List Masjid</a>";
            $data["modul"] = $this->modul;
            $data["url"] = "tambah_data";
            $data["ph"] = "Masjid";
            $data["pd"] = "Form Masjid";
            $this->view("form", $data);
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }

    public function kabupaten()
	{
		$prov_id = $this->uri->segment(3);

		$kabupaten = $this->maps_repository->kabupaten($prov_id);

		echo json_encode($kabupaten);
	}

	public function kecamatan()
	{
		$prov_id = $this->uri->segment(3);

		$kecamatan = $this->maps_repository->kecamatan($prov_id);

		echo json_encode($kecamatan);
	}

	public function kelurahan()
	{
		$prov_id = $this->uri->segment(3);

		$kelurahan = $this->maps_repository->kelurahan($prov_id);

		echo json_encode($kelurahan);
	}
}
