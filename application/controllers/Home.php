<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{
	protected $modul = "home";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_model', "hm");
	}

	public function index()
	{
		$ldata = array();
		if ($this->input->post("btn_filter") === "filter")
		{
			$provinsi = $this->input->post('opt_provinsi');
			$kabupaten = $this->input->post('opt_kabupaten');
			$kecamatan = $this->input->post('opt_kecamatan');
			$kelurahan = $this->input->post('opt_kelurahan');

			if ($provinsi != "")
			{
				$w["MOSQUE_TBL.PROVINSI"] = $provinsi;
			}

			if ($kabupaten != "")
			{
				$w["MOSQUE_TBL.KABUPATEN"] = $kabupaten;
			}

			if ($kecamatan != "")
			{
				$w["MOSQUE_TBL.KECAMATAN"] = $kecamatan;
			}

			if ($kelurahan != "")
			{
				$w["MOSQUE_TBL.KELURAHAN"] = $kelurahan;
			}

			$w[1] = 1;

			$res_kegiatan = $this->hm->get_kegiatan($w)->result();
		}
		else
		{
			$res_kegiatan = $this->hm->get_kegiatan()->result();
		}

		$res_provinsi = $this->hm->get_provinsi()->result();
		$res_keahlian = $this->hm->get_keahlian()->result();
		$data["res_provinsi"] = $res_provinsi;
		$data["res_keahlian"] = $res_keahlian;
		$data["res_kegiatan"] = $res_kegiatan;
		$data["modul"] = $this->modul;

		
		$ldata['content_block'] = $this->load->view('home/home', $data, true);
		$this->load->view('template/template_new', $ldata);
	}

	function ajax_kabupaten()
    {
        $id_provinsi = $this->input->post('id_provinsi');
        $where["provinsiId"] = $id_provinsi;
        $res_kabupaten = $this->hm->get_kabupaten($where)->result();
        $data["res_kabupaten"] = $res_kabupaten;
        echo "<option value=\"\">Pilih Kabupaten</option>";
        $this->load->view('user_registration/v_kabupaten', $data);
        $html = $this->output->get_output();
    }

    function ajax_kecamatan()
    {
        $id_kabupaten = $this->input->post('id_kabupaten');
        $where["kabupatenId"] = $id_kabupaten;
        $res_kecamatan = $this->hm->get_kecamatan($where)->result();
        $data["res_kecamatan"] = $res_kecamatan;
        echo "<option value=\"\">Pilih Kecamatan</option>";
        $this->load->view('user_registration/v_kecamatan', $data);
        $html = $this->output->get_output();
    }

    function ajax_kelurahan()
    {
        $id_kecamatan = $this->input->post('id_kecamatan');
        $where["kecamatanId"] = $id_kecamatan;
        $res_kelurahan = $this->hm->get_kelurahan($where)->result();
        $data["res_kelurahan"] = $res_kelurahan;
        echo "<option value=\"\">Pilih Kelurahan/Desa</option>";
        $this->load->view('user_registration/v_kelurahan', $data);
        $html = $this->output->get_output();
    }
}
