<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MY_Controller
{
	protected $modul = "posts";

	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
		$this->load->model('post_model', 'mm');
	}

	public function index()
	{
		$this->set('post_data', $this->mm->get_data());
		$this->view('home');
	}
	
	public function form_new() 
	{
		$this->view('form');
	}
	
	public function save() 
	{
		$button = $this->input->post("btn_save");
		$arr_data = $this->input->post();
		unset($arr_data['btn_save'], $arr_data['btn_cancel']);
		
		// echo "<pre>";var_dump($arr_data);echo "</pre>";exit;
		
		// Jika data baru maka ...
        if  ($button === "btn_simpan")
        {
			$this->mm->insert_data($arr_data); //echo $this->db->last_query(); exit();
            redirect($this->modul);
		}
		// Jika edit data maka ...
        elseif ($button === "btn_ubah")
        {
            $this->mm->update_data($arr_data, array('post_id' => $id));
            redirect($this->modul);
        }
        else
        {
            redirect($this->modul);
        }
	}

}
