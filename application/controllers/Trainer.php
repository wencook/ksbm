<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trainer extends MY_Controller
{
    var $modul = "trainer";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("trainer_model", "tm");
    }

    function index()
    {
        $data["pr"] = "<a href=\"".site_url($this->modul.'/tambah_data')."\" class=\"btn btn-warning btn-sm\">Tambah Data</a>
        <button type=\"button\" title=\"Refresh\" onclick=\"reload_table();\" class=\"btn btn-warning btn-sm\">Refresh</button>";
        $data["modul"] = $this->modul;
        $data["ph"] = "Trainer";
        $this->view("home", $data);
    }

    public function ajax_list()
    {
        $list = $this->tm->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = $r->INSTRUCTOR;
            $row[] = $r->KEAHLIAN;
            $row[] = $r->TITLE;
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/edit_data/".$r->ID)."\" class=\"btn btn-warning btn-flat btn-xs\">Edit</a>&nbsp;<a href=\"".site_url($this->modul."/delete_data/".$r->ID)."\" class=\"btn btn-danger btn-xs btn-flat\">Delete</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->tm->count_all();
        $output["recordsFiltered"] = $this->tm->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function tambah_data()
    {
        $data["INSTRUCTOR"] = $this->input->post('txt_instructor');
        $data["BIDANG"] = $this->input->post('opt_bidang');
        $data["TITLE"] = $this->input->post('txt_title');
        $data["DESCRIPTION"] = $this->input->post('txt_description');
        $data["KEAHLIAN1"] = $this->input->post('txt_keahlian1');
        $data["KEAHLIAN2"] = $this->input->post('txt_keahlian2');
        $data["KEAHLIAN3"] = $this->input->post('txt_keahlian3');
        $data["KEAHLIAN4"] = $this->input->post('txt_keahlian4');
        $data["KEAHLIAN5"] = $this->input->post('txt_keahlian5');
        $data["KEAHLIAN6"] = $this->input->post('txt_keahlian6');
        $data["KEAHLIAN7"] = $this->input->post('txt_keahlian7');
        $data["KEAHLIAN8"] = $this->input->post('txt_keahlian8');
        $data["KEAHLIAN9"] = $this->input->post('txt_keahlian9');
        $data["KEAHLIAN10"] = $this->input->post('txt_keahlian10');

        if ($this->input->post('btn_save') === "save")
        {
            $simpan = $this->tm->insert_data($data);
            if ($simpan > 0)
            {
                redirect($this->modul, "refresh");
            }
            else
            {
                redirect($this->modul."/tambah_data", "refresh");
            }
        }
        elseif ($this->input->post('btn_save') === "update")
        {
            $where["ID"] = $this->input->post('txt_id');
            $simpan = $this->tm->update_data($data, $where);
            if ($simpan > 0)
            {
                redirect($this->modul, "refresh");
            }
            else
            {
                redirect($this->modul."/tambah_data", "refresh");
            }
        }
        else
        {
            $where["ur.EMAIL"] = $this->session->userdata("userEmail");
            $data["res_keahlian"] = $this->tm->get_keahlian($where)->result();
            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">List Masjid</a>";
            $data["modul"] = $this->modul;
            $data["url"] = "tambah_data";
            $data["ph"] = "Trainer";
            $data["pd"] = "Form Masjid";
            $this->view("form", $data);
        }
    }

    function edit_data($id = NULL)
    {
        if ($id !== NULL)
        {
            $where["ID"] = $id;
            $result = $this->tm->get_trainer($where);
            $wk["ur.EMAIL"] = $this->session->userdata("userEmail");
            $data["res_keahlian"] = $this->tm->get_keahlian($wk)->result();
            $data["r_trainer"] = $result->row();
            $data["pr"] = "<a href=\"".site_url($this->modul)."\" class=\"btn btn-warning btn-sm\">List Masjid</a>";
            $data["modul"] = $this->modul;
            $data["url"] = "tambah_data";
            $data["ph"] = "Trainer";
            $data["pd"] = "Form Masjid";
            $this->view("form", $data);
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }
}
