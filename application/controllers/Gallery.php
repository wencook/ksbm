<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller
{
    protected $modul = "gallery";
    
    public function __construct()
    {
        parent::__construct();
        $this->_cek_login();
        $this->load->helper(array('form'));
        $this->load->model('gallery_model',"mm");
    }

    public function index()
    {
        $data["error"] = '';
        $data["res_gallery"] = $this->mm->get_data();
        $this->view('home', $data);
    }

    public function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
            $data["error"] = $this->upload->display_errors();
            $this->view('home', $data);
        }
        else
        {
            $upload_data = $this->upload->data();
            $data_gallery["gallery_ket"] = $upload_data["file_name"];
            $data_gallery["gallery_path"] = $upload_data["file_path"];
            $this->mm->insert_data($data_gallery);
            redirect('gallery','refresh');
        }
    }

}
