<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends MY_Controller
{
    var $modul = "keuangan";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("keuangan_model", "mrm");
    }

    function index()
    {
        $data["modul"] = $this->modul;
        $data["ph"] = "Keuangan";
        $this->view("home", $data);
    }

    public function ajax_list()
    {
        $list = $this->mrm->get_datatables($this->input->post());
        $data = array();
        $no = $this->input->post('start');
        foreach ($list as $r)
        {
            $no++;
            $row = array();
            $row[] = '<div class="text-center">'.$no.'</div>';
            $row[] = $r->ACCOUNT_SENDER;
            $row[] = $r->NAME_SENDER;
            $row[] = "<div class=\"text-right\">".number_format($r->AMOUNT,0, ",",".")."</div>";
            $row[] = "<div class=\"text-center\"><a href=\"".site_url($this->modul."/edit_data/".$r->ID)."\" class=\"btn btn-warning btn-flat btn-xs\">Received</a></div>";

            $data[] = $row;
        }

        $output["draw"] = $this->input->post('draw');
        $output["recordsTotal"] = $this->mrm->count_all();
        $output["recordsFiltered"] = $this->mrm->count_filtered($this->input->post());
        $output["data"] = $data;
        //output to json format
        echo json_encode($output);
    }

    function edit_data($id = NULL)
    {
        if ($id !== NULL)
        {
            $where["ID"] = $id;
            $data["STATUS"] = 1;
            $result = $this->mrm->update_data($data,$where);

            redirect($this->modul, "refresh");
        }
        else
        {
            redirect($this->modul, "refresh");
        }
    }
}
