<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model
{
    protected $table = null;
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function get_data($return="result", $field="*", $left_join=NULL, $where=NULL, $like=NULL, $order=NULL, $limit=NULL, $distinct=NULL)
    {
        $this->db->select($field);
        $this->db->from($this->table);

        if ($left_join !== NULL)
        {
            foreach ($left_join as $key_join => $val_join)
            {
                $this->db->join($key_join, $val_join, 'left');
            }
        }

        if ($where !== NULL)
        {
            foreach ($where as $key_where => $val_where)
            {
                $this->db->where($key_where, $val_where);
            }
        }

        if ($like !== NULL)
        {
            foreach ($like as $key_like => $val_like)
            {
                $this->db->like($key_like, $val_like);
            }
        }

        if ($order !== NULL)
        {
            foreach($order as $key_order => $val_order)
            {
                $this->db->order_by($key_order, $val_order);
            }
        }

        if ($limit !== NULL)
        {
            foreach ($limit as $key_limit => $val_limit)
            {
                $this->db->limit($key_limit, $val_limit);
            }
        }

        if ($distinct !== NULL)
        {
            $this->db->distinct();
        }

        $qry = $this->db->get();
        // echo $this->db->last_query();

        if ($return === "row")
        {
            return $qry->num_rows() > 0 ? $qry->row() : FALSE;
        }
        else
        {
            return $qry->num_rows() > 0 ? $qry->result() : FALSE;
        }
    }

    /*
     * @param string $table nama table
     * @param string $data array data yang akan diinput beserta nama field
     */
    public function insert_data($data)
    {
        // $sql = $this->db->set($data)->get_compiled_insert($table);
        // echo $sql;exit;
        return $this->db->insert($this->table, $data);
    }

    /*
     * @param string $table nama table
     * @param string $data field dan data yang akan diupdate
     * @param array $field field where
     * @param array $where data yang dicari
     */
    public function update_data($data, $where)
    {
        foreach ($where as $key => $row)
        {
            $this->db->where($key, $row);
        }
        $this->db->update($this->table, $data);
        // echo $this->db->last_query();
    }

    /*
     * @param string $table nama table
     * @param array $where data yang dicari
     */
    public function delete_data($where)
    {
        foreach ($where as $key => $value)
        {

            $this->db->where($key, $value);
        }
        return $this->db->delete($this->table);
    }
    /*
     * @param string $table nama table
     * @param array $like data yang dicari
     */
    public function check_duplicate($like)
    {
        foreach ($like as $key => $row)
        {
            $this->db->like($key, $row, "none");
        }

        $this->db->from($this->table);

        return $this->db->count_all_results();
    }
}
