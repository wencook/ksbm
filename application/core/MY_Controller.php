<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	protected $modul = null;
	protected $data = array();

	function __construct()
	{
		parent::__construct();
        $this->output->enable_profiler(FALSE);
	}

	protected function _cek_login()
	{
		if ($this->session->userdata('isLoggedIn') !== TRUE OR ! $this->session->userdata('isLoggedIn'))
		{
			redirect('auth');
		}
	}

	protected function set($key, $val=false)
	{
		if (is_array($key))
		{
			foreach ($key as $k => $v)
				$this->data[$k] = $v;
		}
		else
		{
			$this->data[$key] = $val;
		}
	}

	protected function view($view, $data=null)
	{
		if (is_array($data))
			foreach($data as $k => $v)
				$this->set($k, $v);

		if (!empty($this->modul))
		{
			$this->set('modul', $this->modul);
			$this->load->view('template/header', $this->data);
			$this->load->view('template/menu', $this->data);
			$this->load->view("{$this->modul}/{$view}", $this->data);
		}
		else
		{
			$this->load->view('template/header', $this->data);
			$this->load->view('template/menu', $this->data);
			$this->load->view($view, $this->data);
		}
		$this->load->view('template/footer');
	}
}
